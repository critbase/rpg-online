from .Special import Action, Special


class Monster:
    def __init__(self):
        self.name = "Monster"
        self.max_hp = 10
        self.hp = 10
        self.defense = 10
        self.reward_xp = 10
        self.level = 1
        self.actions: list[Action] = [Special("attack", 10, 0)]

    def to_dict(self):
        # TODO: don't return all info, depending on what info about monsters we want to hide from players
        return {
            "name": self.name,
            "max_hp": self.max_hp,
            "hp": self.hp,
            "defense": self.defense,
            "reward_xp": self.reward_xp,
            "level": self.level,
        }

    def __repr__(self):
        return f"({self.name}, hp: {self.hp}, def: {self.defense})"


class Slime(Monster):
    def __init__(self):
        self.name = "Slime"
        self.max_hp = 5
        self.hp = 5
        self.defense = 2
        self.reward_xp = 3
        self.level = 1
        self.actions: list[Action] = [Special("attack", 3, 0, False)]


class LargeSlime(Monster):
    def __init__(self):
        self.name = "Large Slime"
        self.max_hp = 7
        self.hp = 7
        self.defense = 4
        self.reward_xp = 5
        self.level = 2
        self.actions: list[Action] = [Special("attack", 5, 0, False)]


class SlimeQueen(Monster):
    def __init__(self):
        self.name = "Queen Slime"
        self.max_hp = 10
        self.hp = 10
        self.defense = 5
        self.reward_xp = 10
        self.level = 3
        self.actions: list[Action] = [
            Special("attack", 7, 0, False),
            Special("splash", 10, 0, False),
        ]


class Golem(Monster):
    def __init__(self):
        self.name = "Golem"
        self.max_hp = 10
        self.hp = 10
        self.defense = 7
        self.reward_xp = 20
        self.level = 4
        self.actions: list[Action] = [
            Special("attack", 10, 0, False),
            Special("slam", 15, 0, False),
            Special("miss", 0, 0, False),
            Special("miss", 0, 0, False),
        ]


class Skeleton(Monster):
    def __init__(self):
        self.name = "Skeleton"
        self.max_hp = 8
        self.hp = 8
        self.defense = 4
        self.reward_xp = 10
        self.level = 3
        self.actions: list[Action] = [Special("attack", 10, 0, False)]


class SkeletonGuard(Monster):
    def __init__(self):
        self.name = "Skeleton Guard"
        self.max_hp = 8
        self.hp = 8
        self.defense = 10
        self.reward_xp = 20
        self.level = 5
        self.actions: list[Action] = [
            Special("attack", 7, 0, False),
            Special("shieldbash", 14, 0, False),
        ]


class SkeletonKnight(Monster):
    def __init__(self):
        self.name = "Skeleton Knight"
        self.max_hp = 20
        self.hp = 20
        self.defense = 6
        self.reward_xp = 30
        self.level = 6
        self.actions: list[Action] = [
            Special("attack", 15, 0, False),
            # TODO: add armor piercing attribute after getting battles fully working
            Special("piercer", 10, 0, True),
        ]


class SkeletonRanger(Monster):
    def __init__(self):
        self.name = "Skeleton Ranger"
        self.max_hp = 4
        self.hp = 4
        self.defense = 7
        self.reward_xp = 30
        self.level = 6
        self.actions: list[Action] = [
            Special("attack", 10, 0, False),
            # snipe pierces armor
            Special("snipe", 20, 0, True),
            Special("miss", 0, 0, False),
            Special("attack", 10, 0, False),
        ]
