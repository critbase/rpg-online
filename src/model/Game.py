# main entrypoint for the game
# 'controller' for the game; the ui_controller instantiates this and calls the non-private methods to pass in user input.
# this class then generates events with information about the game's current state that the ui_controller will probably want to display.
import random

from .Player import Player, PlayerState
from .WorldMap import WorldMap, GRID_HEIGHT, GRID_WIDTH
from .Tile import Tile
from .Battle import Battle, BattleAction
from model.Special import *
from .Monster import *
from .Events import *
from .Errors import *


class Game:
    def __init__(
        self,
        observer: GameObserver,
        world_width: int = GRID_WIDTH,
        world_height: int = GRID_HEIGHT,
    ):
        self.players: dict[str, Player] = dict()
        self.world = WorldMap(world_width, world_height)
        self.observer = observer

    ### non-private methods; meant to be called externally by ui_controller

    # called before doing anything else
    def start(self):
        # if we need to add initialization, we can do it here
        self.observer(GameStartedEvent(world=self.world.to_dict()))

    def get_player_as_dict(self, uid: str):
        # FIXME: error handling? probably good to have just in case...
        return self.players[uid].to_dict()

    def add_new_player(self, uid: str, player_name: str):
        center = self.world.get_center()
        current_player = Player(player_name, center[0])
        self.players[uid] = current_player
        center[1].add_player(current_player)
        self.observer(
            PlayerJoinEvent(
                player_name=current_player.name,
                player_level=current_player.level,
                world=self.world.to_dict(),
                player_pos=current_player.position,
                player_hp=(current_player.hp, current_player.max_hp),
                specials=[s.name for s in current_player.special],
                other_players={
                    self.players[p].name: self.players[p].position for p in self.players
                },
            )
        )

    # we shouldn't handle data storage in the model; instead, store players... somewhere and add them with this function
    def add_existing_player(self, uid: str, player: Player):
        self.players[uid] = player
        self.world[player.position].add_player(player)
        self.observer(
            PlayerJoinEvent(
                player_name=player.name,
                player_level=player.level,
                world=self.world.to_dict(),
                player_pos=player.position,
                player_hp=(player.hp, player.max_hp),
                specials=[s.name for s in player.special],
                other_players={
                    self.players[p].name: self.players[p].position for p in self.players
                },
            )
        )

    def remove_player(self, uid: str):
        if uid in self.players:
            p = self.players.pop(uid)
            self.observer(PlayerLeaveEvent(player_name=uid))
            return p

    # move the player in the selected direction
    def move_player(self, player_name: str, direction: Direction):
        # first, get the player we're referencing, or return an error if we can't find them
        current_player: Player | None = None
        if player_name in self.players:
            current_player = self.players[player_name]
        else:
            raise PlayerNotFoundError()

        # check if they're in the correct state
        if current_player.state != PlayerState.MAP:
            raise PlayerInvalidActionError()

        # check if the space in the proposed direction is valid
        nextpos = self._pos_in_direction(current_player.position, direction)
        if not self.world.check_valid_pos(nextpos):
            raise PlayerInvalidPositionError()

        # finally, if we get here, we can move the player and check the battle conditions
        self.world[current_player.position].remove_player(current_player)
        current_player.position = nextpos
        next_tile = self.world[nextpos]
        next_tile.add_player(current_player)
        self.observer(
            PlayerMoveEvent(
                newposition=nextpos,
                player_name=player_name,
                direction=direction.name,
            )
        )

        # check if there is an existing battle on the tile
        if next_tile.battle is not None:
            self._join_battle(current_player, next_tile)
            self.observer(
                PlayerJoinBattleEvent(
                    players=[player.name for player in next_tile.battle.players],
                    monster=next_tile.battle.monster.to_dict(),
                    position=current_player.position,
                    player_name=player_name,
                    player_hp=(current_player.hp, current_player.max_hp),
                    player_mp=(current_player.mp, current_player.max_mp),
                )
            )
            self.observer(
                BattleTurnAdvancedEvent(
                    players=[player.name for player in next_tile.battle.players],
                    monster=next_tile.battle.monster.to_dict(),
                    turnindex=next_tile.battle.current_turn,
                    position=current_player.position,
                )
            )
            # if there isn't already a battle, roll a 20% chance to create a battle
        elif self._roll_battle_chance():
            self._create_battle(current_player, next_tile, self._get_random_monster())
            self.observer(
                BattleCreatedEvent(
                    nextpos, next_tile.battle.monster.to_dict(), player_name
                )
            )
            self.observer(
                PlayerJoinBattleEvent(
                    players=[player.name for player in next_tile.battle.players],
                    monster=next_tile.battle.monster.to_dict(),
                    position=current_player.position,
                    player_name=player_name,
                    player_hp=(current_player.hp, current_player.max_hp),
                    player_mp=(current_player.mp, current_player.max_mp),
                )
            )
            self.observer(
                BattleTurnAdvancedEvent(
                    players=[player.name for player in next_tile.battle.players],
                    monster=next_tile.battle.monster.to_dict(),
                    turnindex=next_tile.battle.current_turn,
                    position=current_player.position,
                )
            )

    def battle_action(
        self, player_name: str, action: BattleAction, special_name: str = ""
    ):
        current_player: Player | None = None
        if player_name in self.players:
            current_player = self.players[player_name]
        else:
            raise PlayerNotFoundError()

        # check if they're in the correct state
        if current_player.state != PlayerState.BATTLE:
            raise PlayerInvalidActionError()

        # get the battle
        current_tile = self.world[current_player.position]
        current_battle = current_tile.battle
        if current_battle is None:
            raise BattleNotFoundError()
        # check if it's even this player's turn
        if current_battle.current_player() is not current_player:
            raise OutOfTurnError()

        # now, actually parse and validate input
        match action:
            case BattleAction.ATTACK:
                # make sure higher defense values don't 'heal' the monster
                damage = max(current_player.attack - current_battle.monster.defense, 1)
                current_battle.monster.hp = current_battle.monster.hp - damage
                self.observer(
                    BattlePlayerATTACKEvent(
                        player=current_player.name,
                        monster=current_battle.monster.to_dict(),
                        damage=damage,
                        position=current_player.position,
                    )
                )
            case BattleAction.SPECIAL:
                # find the special in the player's list
                selected_special = None
                for s in current_player.special:
                    if s.name == special_name:
                        selected_special = s
                        break
                if selected_special is None:
                    raise SpecialNotFoundError()

                if isinstance(selected_special, Heal):
                    current_player.hp = min(
                        current_player.hp + selected_special.heal,
                        current_player.max_hp,
                    )
                elif isinstance(selected_special, Special):
                    if current_player.mp < selected_special.cost:
                        raise SpecialCostsTooMuchError()
                    else:
                        if selected_special.pierce:
                            current_battle.monster.hp = current_battle.monster.hp - max(
                                round(current_player.attack * selected_special.damage),
                                1,
                            )
                        else:
                            current_battle.monster.hp = current_battle.monster.hp - max(
                                round(current_player.attack * selected_special.damage)
                                - current_battle.monster.defense,
                                1,
                            )
                current_player.mp -= selected_special.cost
                self.observer(
                    BattlePlayerSPECIALEvent(
                        player=current_player.name,
                        monster=current_battle.monster.to_dict(),
                        selected_special=selected_special.to_dict(),
                        position=current_player.position,
                        player_mp=(current_player.mp, current_player.max_mp),
                    )
                )
            case BattleAction.PASS:
                self.observer(
                    BattlePlayerPASSEvent(
                        player=current_player.name,
                        monster=current_battle.monster.to_dict(),
                        position=current_player.position,
                    )
                )
                pass
            case BattleAction.LEAVE:
                # decrement the turn count so that we expect an action from the correct player
                current_battle.current_turn -= 1
                current_battle.players.remove(current_player)
                current_player.state = PlayerState.MAP
                self.observer(
                    BattlePlayerLEAVEEvent(
                        monster=current_battle.monster.to_dict(),
                        player=current_player.name,
                        position=current_player.position,
                    )
                )

        # check end conditions
        if current_battle.monster.hp <= 0:
            # win condition
            self.observer(
                BattleWonEvent(
                    players=[player.name for player in current_battle.players],
                    monster=current_battle.monster.to_dict(),
                    position=current_player.position,
                )
            )
            for player in current_battle.players[:]:
                player.state = PlayerState.MAP
                self._player_add_experience(player, current_battle.monster.reward_xp)
                current_battle.players.remove(player)
                player.hp = min(player.hp + round(player.max_hp / 4), player.max_hp)
                player.mp = min(player.mp + round(player.max_mp / 4), player.max_mp)
            current_tile.remove_battle()
            return
        elif len(current_battle.players) <= 0:
            # lose condition
            self.observer(
                BattleLostEvent(
                    players=[player.name for player in current_battle.players],
                    monster=current_battle.monster.to_dict(),
                    position=current_player.position,
                )
            )
            for player in current_battle.players[:]:
                player.state = PlayerState.MAP
                current_battle.players.remove(player)
            current_tile.remove_battle()
            return

        # advance turn
        if current_battle.current_turn >= (len(current_battle.players) - 1):
            current_battle.current_turn = len(current_battle.players)
            # perform monster's turn here
            target_player = random.choice(current_battle.players)
            current_attack = random.choice(current_battle.monster.actions)

            # TODO: check the type of current_attack... though we don't have to worry about that for now
            if type(current_attack) is Special:
                if current_attack.pierce:
                    monster_damage = round(max(current_attack.damage, 0))
                    target_player.hp = target_player.hp - monster_damage
                    self.observer(
                        BattleMonsterAttackEvent(
                            position=current_player.position,
                            target_player=current_player.name,
                            target_player_hp=current_player.hp,
                            monster=current_battle.monster.to_dict(),
                            attack=current_attack.name,
                            attack_damage=monster_damage,
                        )
                    )
                else:
                    monster_damage = max(
                        round(current_attack.damage) - target_player.defense, 0
                    )
                    target_player.hp = target_player.hp - monster_damage
                    self.observer(
                        BattleMonsterAttackEvent(
                            position=current_player.position,
                            target_player=current_player.name,
                            target_player_hp=current_player.hp,
                            monster=current_battle.monster.to_dict(),
                            attack=current_attack.name,
                            attack_damage=monster_damage,
                        )
                    )
            # TODO: check whether the player is dead...
            if target_player.hp <= 0:
                current_battle.players.remove(target_player)
                target_player.hp = 1
                target_player.state = PlayerState.MAP
                self.observer(
                    BattlePlayerDiedEvent(
                        player=target_player.name,
                        position=target_player.position,
                    )
                )

                # check if the last player just died
                if len(current_battle.players) <= 0:
                    self.observer(
                        BattleLostEvent(
                            monster=current_battle.monster.to_dict(),
                            players=[player.name for player in current_battle.players],
                            position=current_player.position,
                        )
                    )
                    current_tile.remove_battle()

            current_battle.current_turn = 0
        else:
            current_battle.current_turn += 1

        self.observer(
            BattleTurnAdvancedEvent(
                players=[player.name for player in current_battle.players],
                monster=current_battle.monster.to_dict(),
                turnindex=current_battle.current_turn,
                position=current_player.position,
            )
        )

    def player_status(self, player_name: str):
        if player_name not in self.players:
            raise PlayerNotFoundError()
        else:
            current_player = self.players[player_name]
            self.observer(PlayerStatusRequestEvent(player=current_player.to_dict()))

    def change_player_name(self, player_name: str, new_player_name: str):
        if player_name not in self.players:
            raise PlayerNotFoundError()
        else:
            current_player = self.players[player_name]
            self.players[new_player_name] = self.players.pop(player_name)
            current_player.name = new_player_name
            self.observer(
                PlayerNicknameEvent(old_name=player_name, new_name=new_player_name)
            )

    def send_message(self, player_name: str, message: str):
        if player_name not in self.players:
            raise PlayerNotFoundError()
        else:
            self.observer(
                PlayerChatEvent(
                    message=message, player_name=self.players[player_name].name
                )
            )

    ### debug methods. these are only here to facilitate easier testing

    def debug_give_player_xp(self, player_name: str, xp_amount: int):
        if player_name not in self.players:
            raise PlayerNotFoundError()
        else:
            current_player = self.players[player_name]
            self._player_add_experience(current_player, xp_amount)
        pass

    def debug_create_battle(self, player_name: str, monster_type: Monster):
        if player_name not in self.players:
            raise PlayerNotFoundError()
        else:
            current_player = self.players[player_name]
            if current_player.state != PlayerState.MAP:
                raise PlayerInvalidActionError()
            on_tile = self.world[current_player.position]
            self._create_battle(current_player, on_tile, monster_type)
            self.observer(
                BattleCreatedEvent(
                    monster=on_tile.battle.monster.to_dict(),
                    newposition=current_player.position,
                    player_name=current_player.name,
                )
            )
            self.observer(
                PlayerJoinBattleEvent(
                    position=current_player.position,
                    player_name=current_player.name,
                    monster=on_tile.battle.monster.to_dict(),
                    players=[player.name for player in on_tile.battle.players],
                    player_hp=(current_player.hp, current_player.max_hp),
                    player_mp=(current_player.mp, current_player.max_mp),
                )
            )
            self.observer(
                BattleTurnAdvancedEvent(
                    players=[player.name for player in on_tile.battle.players],
                    monster=on_tile.battle.monster.to_dict(),
                    turnindex=on_tile.battle.current_turn,
                    position=current_player.position,
                )
            )

    ### private methods; should not be called externally. mostly helper functions

    # cur_pos should be (x, y)
    def _pos_in_direction(self, cur_pos: tuple[int, int], direction: Direction):
        match direction:
            case Direction.UP:
                return (cur_pos[0], cur_pos[1] + 1)
            case Direction.DOWN:
                return (cur_pos[0], cur_pos[1] - 1)
            case Direction.RIGHT:
                return (cur_pos[0] + 1, cur_pos[1])
            case Direction.LEFT:
                return (cur_pos[0] - 1, cur_pos[1])

    def _join_battle(self, current_player: Player, on_tile: Tile):
        current_player.state = PlayerState.BATTLE
        if on_tile.battle is not None:
            on_tile.battle.add_player(current_player)
        else:
            raise BattleNotFoundError()

    def _roll_battle_chance(self) -> bool:
        chance = random.randint(0, 6)
        if chance == 0:
            return True
        else:
            return False

    def _create_battle(self, current_player: Player, on_tile: Tile, monster: Monster):
        current_player.state = PlayerState.BATTLE
        on_tile.battle = Battle(current_player, monster)

    def _player_add_experience(self, current_player: Player, add_xp: int):
        current_player.xp += add_xp
        while current_player.xp >= 10:
            current_player.xp -= 10
            current_player.level += 1

            # increase stats
            current_player.max_hp += 10
            current_player.hp = current_player.max_hp
            current_player.max_mp += 5
            current_player.mp = current_player.max_mp
            current_player.attack += 1
            current_player.defense += 1

            self.observer(
                PlayerLevelUpEvent(
                    level=current_player.level, player_name=current_player.name
                )
            )
            if current_player.level == 2:
                lowheal = LowHeal()
                current_player.special.append(lowheal)
                self.observer(
                    PlayerGainSkillEvent(
                        skill_name=lowheal.name, player_name=current_player.name
                    )
                )
            if current_player.level == 3:
                doubleattack = DoubleAttack()
                current_player.special.append(doubleattack)
                self.observer(
                    PlayerGainSkillEvent(
                        skill_name=doubleattack.name, player_name=current_player.name
                    )
                )
            if current_player.level == 4:
                piercer = Piercer()
                current_player.special.append(piercer)
                self.observer(PlayerGainSkillEvent(piercer.name, current_player.name))
            if current_player.level == 5:
                tripleattack = TripleAttack()
                current_player.special.append(tripleattack)
                self.observer(
                    PlayerGainSkillEvent(tripleattack.name, current_player.name)
                )

    def _get_random_monster(self) -> Monster:
        # List of available monster classes
        monster_classes = [
            Slime,
            LargeSlime,
            SlimeQueen,
            Golem,
            Skeleton,
            SkeletonGuard,
            SkeletonKnight,
            SkeletonRanger,
        ]

        # Randomly select a monster class
        selected_monster_class = random.choices(
            monster_classes, [40, 20, 10, 5, 10, 5, 5, 5], k=1
        )

        # Create an instance of the selected monster class
        return selected_monster_class[0]()
