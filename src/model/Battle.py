from enum import Enum
from .Player import Player
from .Monster import Monster


BattleAction = Enum("BattleAction", ["ATTACK", "SPECIAL", "PASS", "LEAVE"])


class Battle:
    def __init__(
        self,
        firstplayer: Player,
        monster: Monster,
    ):
        self.players: list[Player] = list()
        self.players.append(firstplayer)
        self.monster: Monster = monster
        self.current_turn: int = 0

    def add_player(self, current_player: Player):
        self.players.append(current_player)

    def current_player(self):
        return self.players[self.current_turn]

    def __repr__(self) -> str:
        return f"battle: {len(self.players)} players fighting a {self.monster}"
