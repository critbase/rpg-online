from enum import Enum
from typing import Any

from .Special import Action, SpecialTypes

PlayerState = Enum("PlayerState", ["MAP", "BATTLE"])


# data for the player, like the grid position its on, hp/mp, in a random encounter or not, maybe more?
class Player:
    def __init__(self, name: str, startingpos: tuple[int, int]):
        self.name: str = name

        self.max_hp: int = 10
        self.hp: int = 10
        self.max_mp: int = 10
        self.mp: int = 10
        self.xp: int = 0
        self.level: int = 1
        self.attack: int = 5
        self.defense: int = 2
        self.special: list[Action] = list()

        # default state is in the map
        self.state = PlayerState.MAP
        # this should be (x, y)
        self.position: tuple[int, int] = startingpos

    def enter_battle(self):
        self.state = PlayerState.BATTLE

    def exit_battle(self):
        self.state = PlayerState.MAP

    def to_dict(self):
        print(f"converting player {self.name} to dict")
        player_as_dict: dict[str, Any] = {
            "state": self.state.name,
            "position": self.position,
            "name": self.name,
            "hp": {"current": self.hp, "max": self.max_hp},
            "mp": {"current": self.mp, "max": self.max_mp},
            "level": self.level,
            "xp": self.xp,
            "atk": self.attack,
            "def": self.defense,
            "specials": [s.name for s in self.special],
        }

        return player_as_dict

    # create and return a player initialized from a dictionary of the same format returned when a player is serialized to a dictionary
    @staticmethod
    def from_dict(custom_data: dict[str, Any]):
        print("converting dict to player")
        p = Player(
            name=custom_data["name"],
            startingpos=(custom_data["position"][0], custom_data["position"][1]),
        )
        p.max_hp = custom_data["hp"]["max"]
        p.hp = custom_data["hp"]["current"]
        p.max_mp = custom_data["mp"]["max"]
        p.mp = custom_data["mp"]["current"]
        p.level = custom_data["level"]
        p.xp = custom_data["xp"]
        p.attack = custom_data["atk"]
        p.defense = custom_data["def"]
        for s in custom_data["specials"]:
            p.special.append(SpecialTypes[s]())

        return p

    def __repr__(self):
        return f"(Player {self.name}, xp {self.xp}/100, level {self.level}, hp {self.hp}/{self.max_hp}, mp {self.mp}/{self.max_mp}, attack: {self.attack}, defense: {self.defense}, in state {self.state})"
