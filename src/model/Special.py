from typing import Any


class Action:
    def __init__(self, name: str):
        self.name = name
        self.cost = 0

    def to_dict(self) -> dict[str, Any]:
        return {"type": "action", "name": self.name}


class Special(Action):
    def __init__(self, name: str, damage: int | float, cost: int, pierce: bool = False):
        self.name = name
        # use this "damage" stat as a multiplier on the player's attack
        self.damage = damage
        self.cost = cost
        self.pierce = pierce

    def to_dict(self):
        return {
            "type": "special",
            "name": self.name,
            "damage": self.damage,
            "cost": self.cost,
        }

    def __repr__(self) -> str:
        return f"(name: {self.name}, damage: {self.damage}, cost: {self.cost})"


class Heal(Action):
    def __init__(self, name: str, heal: int, cost: int):
        self.name = name
        self.heal = heal
        self.cost = cost

    def to_dict(self):
        return {
            "type": "heal",
            "name": self.name,
            "heal": self.heal,
            "cost": self.cost,
        }

    def __repr__(self) -> str:
        return f"(name: {self.name}, heal: {self.heal}, cost: {self.cost})"


class LowHeal(Heal):
    def __init__(self):
        super().__init__("LowHeal", 5, 3)


class DoubleAttack(Special):
    def __init__(self):
        super().__init__("DoubleAttack", 2, 3, False)


class Piercer(Special):
    def __init__(self):
        super().__init__("Piercer", 1.3, 1, True)


class TripleAttack(Special):
    def __init__(self):
        super().__init__("TripleAttack", 3, 5, False)


SpecialTypes = {
    "LowHeal": LowHeal,
    "DoubleAttack": DoubleAttack,
    "Piercer": Piercer,
    "TripleAttack": TripleAttack,
}
