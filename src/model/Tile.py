from .Player import Player
from .Battle import Battle


# contains info on what is on the tile currently (what players are on it), whether it can be moved into by a player, whether a battle is going on in it
class Tile:
    def __init__(self, valid: bool):
        self.players: list[Player] = list()
        self.battle: Battle | None = None
        self.valid = valid

    def add_player(self, current_player: Player):
        self.players.append(current_player)

    def remove_player(self, current_player: Player):
        # FIXME: handle potential exceptions
        self.players.remove(current_player)

    def remove_battle(self):
        self.battle = None

    def __repr__(self):
        return f"(Tile with players: {self.players}, battle: {self.battle}, valid: {self.valid})"
