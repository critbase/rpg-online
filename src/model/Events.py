from dataclasses import dataclass
from enum import Enum
from typing import Callable, Union, Any

Direction = Enum("Direction", ["UP", "DOWN", "LEFT", "RIGHT"])


@dataclass(eq=True, frozen=True)
class GameStartedEvent:
    # (x,y) -> bool of whether that position is valid. for now, this is enough.
    # in the future, we'd put in relevant tile data as a dict at each position.
    event_name = "GameStartedEvent"
    world: dict[str, bool]


@dataclass(eq=True, frozen=True)
class PlayerMoveEvent:
    event_name = "PlayerMoveEvent"
    newposition: tuple[int, int]
    player_name: str
    direction: str


@dataclass(eq=True, frozen=True)
class PlayerJoinBattleEvent:
    event_name = "PlayerJoinBattleEvent"
    players: list[str]
    monster: dict[str, Any]
    position: tuple[int, int]
    player_name: str
    player_hp: tuple[int, int]
    player_mp: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattleCreatedEvent:
    event_name = "BattleCreatedEvent"
    newposition: tuple[int, int]
    monster: dict[str, Any]
    player_name: str


@dataclass(eq=True, frozen=True)
class PlayerJoinEvent:
    event_name = "PlayerJoinEvent"
    player_name: str
    player_hp: tuple[int, int]
    player_level: int
    player_pos: tuple[int, int]
    world: dict[str, bool]
    specials: list[str]
    other_players: dict[str, tuple[int, int]]


@dataclass(eq=True, frozen=True)
class PlayerLeaveEvent:
    event_name = "PlayerLeaveEvent"
    player_name: str


@dataclass(eq=True, frozen=True)
class PlayerNicknameEvent:
    event_name = "PlayerNicknameEvent"
    old_name: str
    new_name: str


@dataclass(eq=True, frozen=True)
class PlayerChatEvent:
    event_name = "PlayerChatEvent"
    message: str
    player_name: str


@dataclass(eq=True, frozen=True)
class PlayerLevelUpEvent:
    event_name = "PlayerLevelUpEvent"
    level: int
    player_name: str


@dataclass(eq=True, frozen=True)
class PlayerGainSkillEvent:
    event_name = "PlayerGainSkillEvent"
    skill_name: str
    player_name: str


@dataclass(eq=True, frozen=True)
class BattleWonEvent:
    event_name = "BattleWonEvent"
    players: list[str]
    monster: dict[str, Any]
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattleLostEvent:
    event_name = "BattleLostEvent"
    players: list[str]
    monster: dict[str, Any]
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattleMonsterAttackEvent:
    event_name = "BattleMonsterAttackEvent"
    target_player: str
    target_player_hp: int
    monster: dict[str, Any]
    attack: str
    attack_damage: int
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattlePlayerATTACKEvent:
    event_name = "BattlePlayerATTACKEvent"
    player: str
    monster: dict[str, Any]
    damage: int
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattlePlayerSPECIALEvent:
    event_name = "BattlePlayerSPECIALEvent"
    player: str
    player_mp: tuple[int, int]
    monster: dict[str, Any]
    selected_special: dict[str, Any]
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattlePlayerPASSEvent:
    event_name = "BattlePlayerPASSEvent"
    player: str
    monster: dict[str, Any]
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattlePlayerLEAVEEvent:
    event_name = "BattlePlayerLEAVEEvent"
    player: str
    monster: dict[str, Any]
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class BattlePlayerDiedEvent:
    event_name = "BattlePlayerDiedEvent"
    player: str
    position: tuple[int, int]


@dataclass(eq=True, frozen=True)
class PlayerStatusRequestEvent:
    event_name = "PlayerStatusRequestEvent"
    player: dict[str, Any]


@dataclass(eq=True, frozen=True)
class BattleTurnAdvancedEvent:
    event_name = "BattleTurnAdvancedEvent"
    players: list[str]
    monster: dict[str, Any]
    turnindex: int
    position: tuple[int, int]


GameEvent = Union[
    GameStartedEvent,
    PlayerMoveEvent,
    PlayerJoinBattleEvent,
    BattleCreatedEvent,
    PlayerJoinEvent,
    PlayerLeaveEvent,
    PlayerNicknameEvent,
    PlayerChatEvent,
    PlayerLevelUpEvent,
    PlayerGainSkillEvent,
    BattleWonEvent,
    BattleLostEvent,
    BattleMonsterAttackEvent,
    BattlePlayerATTACKEvent,
    BattlePlayerSPECIALEvent,
    BattlePlayerPASSEvent,
    BattlePlayerLEAVEEvent,
    BattlePlayerDiedEvent,
    PlayerStatusRequestEvent,
    BattleTurnAdvancedEvent,
]
GameObserver = Callable[[GameEvent], None]
