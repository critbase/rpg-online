# custom exceptions/errors defined here
class PlayerNotFoundError(Exception):
    pass


class PlayerInvalidActionError(Exception):
    pass


class PlayerInvalidPositionError(Exception):
    pass


class OutOfTurnError(Exception):
    pass


class SpecialNotFoundError(Exception):
    pass


class SpecialCostsTooMuchError(Exception):
    pass


class BattleNotFoundError(Exception):
    pass
