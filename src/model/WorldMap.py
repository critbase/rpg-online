from .Tile import Tile

# 2d array of tiles
GRID_HEIGHT = 32
GRID_WIDTH = 32


class WorldMap:
    def __init__(self, width: int = GRID_WIDTH, height: int = GRID_HEIGHT):
        # store these for ease of access
        self._width = width
        self._height = height
        # use a 1D array to represent the grid; access this with x,y coords through the WorldMap
        self._grid: dict[tuple[int, int], Tile] = dict()
        for x in range(width):
            for y in range(height):
                self._grid[(x, y)] = Tile(True)

    def get_center(self) -> tuple[tuple[int, int], Tile]:
        centerpos = (self._width // 2, self._height // 2)
        centertile = self[centerpos]
        return (centerpos, centertile)

    # pos -> (x, y)
    def check_valid_pos(self, pos: tuple[int, int]) -> bool:
        if pos in self._grid:
            return self[pos[0], pos[1]].valid
        else:
            return False

    # convert the tuple of (x,y) to a string because of JSON...
    def to_dict(self) -> dict[str, bool]:
        worlddict: dict[str, bool] = dict()
        for pos in self._grid:
            worlddict[str(pos)] = self._grid[pos].valid
        return worlddict

    def __getitem__(
        self,
        pos: tuple[int, int],
    ):
        # pos should be (x, y)
        return self._grid[pos]
