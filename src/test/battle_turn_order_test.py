from pytest import fixture, raises
from model.Battle import BattleAction
from model.Game import Game, GameEvent, Direction
from model.Monster import Slime


def handle_events(event: GameEvent):
    print(event)


@fixture
def battle_with_2_players():
    game = Game(2, handle_events, 10, 10)
    game.start()
    centerpos, centertile = game.world.get_center()
    game.debug_create_battle("0", Slime())
    game._join_battle(game.players["1"], centertile)
    return game


def test_turn_order_2_players_0_leave(battle_with_2_players: Game):
    game = battle_with_2_players
    centerpos, centertile = game.world.get_center()
    assert centertile.battle is not None
    assert centerpos == (5, 5)

    game.battle_action("0", BattleAction.LEAVE)
    assert len(centertile.battle.players) == 1
    assert centertile.battle.current_turn == 0
    assert centertile.battle.current_player().name == "1"
    # below assertion tests that the monster did NOT take a turn
    assert (
        centertile.battle.current_player().hp
        == centertile.battle.current_player().max_hp
    )


def test_turn_order_2_players_1_leave(battle_with_2_players: Game):
    game = battle_with_2_players
    centerpos, centertile = game.world.get_center()
    assert centertile.battle is not None
    assert centerpos == (5, 5)

    game.battle_action("0", BattleAction.PASS)
    game.battle_action("1", BattleAction.LEAVE)
    assert len(centertile.battle.players) == 1
    assert centertile.battle.current_turn == 0
    assert centertile.battle.current_player().name == "0"
    # below assertion tests that the monster took a turn.
    assert (
        centertile.battle.current_player().hp
        < centertile.battle.current_player().max_hp
    )


@fixture
def battle_with_3_players():
    game = Game(3, handle_events, 10, 10)
    game.start()
    centerpos, centertile = game.world.get_center()
    game.debug_create_battle("0", Slime())
    game._join_battle(game.players["1"], centertile)
    game._join_battle(game.players["2"], centertile)
    return game


def test_turn_order_3_players_0_leave(battle_with_3_players: Game):
    game = battle_with_3_players
    centerpos, centertile = game.world.get_center()
    assert centertile.battle is not None
    assert centerpos == (5, 5)

    game.battle_action("0", BattleAction.LEAVE)
    assert len(centertile.battle.players) == 2
    assert centertile.battle.current_turn == 0
    assert centertile.battle.current_player().name == "1"
    # below assertion tests that the monster did NOT take a turn
    for player in centertile.battle.players:
        assert player.hp == player.max_hp

    game.battle_action("1", BattleAction.PASS)
    assert centertile.battle.current_turn == 1
    assert centertile.battle.current_player().name == "2"
    for player in centertile.battle.players:
        assert player.hp == player.max_hp


def test_turn_order_3_players_1_leave(battle_with_3_players: Game):
    game = battle_with_3_players
    centerpos, centertile = game.world.get_center()
    assert centertile.battle is not None
    assert centerpos == (5, 5)

    game.battle_action("0", BattleAction.PASS)
    game.battle_action("1", BattleAction.LEAVE)
    assert len(centertile.battle.players) == 2
    assert centertile.battle.current_turn == 1
    assert centertile.battle.current_player().name == "2"
    for player in centertile.battle.players:
        assert player.hp == player.max_hp


def test_turn_order_3_players_2_leave(battle_with_3_players: Game):
    game = battle_with_3_players
    centerpos, centertile = game.world.get_center()
    assert centertile.battle is not None
    assert centerpos == (5, 5)

    game.battle_action("0", BattleAction.PASS)
    game.battle_action("1", BattleAction.PASS)
    game.battle_action("2", BattleAction.LEAVE)
    assert len(centertile.battle.players) == 2
    assert centertile.battle.current_turn == 0
    assert centertile.battle.current_player().name == "0"
    # below assertion tests that the monster took a turn.
    one_player_took_damage = False
    for player in centertile.battle.players:
        if player.hp < player.max_hp:
            one_player_took_damage = True
            break

    assert one_player_took_damage
