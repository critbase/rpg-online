import argparse

from .ui_controller import UIController


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("num_players", type=int, help="number of players")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    controller = UIController(args.num_players)
    controller.run()
