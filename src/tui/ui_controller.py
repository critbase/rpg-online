import random
from model.Game import Game
from model.Player import Player, PlayerState
from model.WorldMap import WorldMap
from model.Tile import Tile
from model.Battle import Battle, BattleAction
from model.Monster import Slime

from model.Events import *
from model.Errors import *
from tui.ui_errors import *


class UIController:
    def __init__(self, num_players: int):
        self.game = Game(num_players, self.handle_event, 10, 10)

    def handle_event(self, event: GameEvent):
        match event:
            case GameStartedEvent():
                print(
                    f"Game started with world map of size {event.world._width}x{event.world._height}"
                )
            case PlayerMoveEvent():
                print(f"{event.player_name} has moved to {event.newposition}")
            case PlayerJoinBattleEvent():
                print(
                    f"{event.player_name} has joined a battle at {event.position} against{event.battle.monster}"
                )
            case BattleCreatedEvent():
                print(
                    f"{event.player_name} has found a new battle at {event.newposition} against {event.battle.monster}"
                )
                print(f"{event.battle.monster.name} has {event.battle.monster.hp} hp")
            case PlayerJoinEvent():
                print(f"{event.player_name} has joined!")
            case PlayerLeaveEvent():
                print(f"{event.player_name} has left!")
                pass
            case PlayerNicknameEvent():
                print(f"{event.old_name} has changed their name to {event.new_name}")
            case PlayerChatEvent():
                print(f"{event.player_name}: {event.message}")
            case PlayerLevelUpEvent():
                print(f"{event.player.name} has leveled up to level {event.level}")
                print(f"their stats are now: {event.player}")
            case PlayerGainSkillEvent():
                print(f"{event.player_name} has gained the skill {event.skill_name}")
            # TODO: add the location of the battle to these events? since only one battle can be at a given position, it can be used as a unique identifier
            case BattleWonEvent():
                print(
                    f"players {[player.name for player in event.players]} have won a battle against {event.monster.name}"
                )
            case BattleLostEvent():
                print(
                    f"players {[player.name for player in event.players]} have lost a battle against {event.monster.name}"
                )
            case BattleMonsterAttackEvent():
                print(
                    f"{event.battle.monster.name} used {event.attack.name} on {event.target_player.name}"
                )
                print(
                    f"{event.target_player.name}'s stats are now: {event.target_player}"
                )
            case BattlePlayerATTACKEvent():
                print(
                    f"{event.player.name} is attacking {event.battle.monster} for {event.damage} damage"
                )
                print(
                    f"monster {event.battle.monster.name}'s health is now: {event.battle.monster.hp}"
                )
            case BattlePlayerSPECIALEvent():
                # TODO: return a different message based on the special type (heal or attack)
                print(
                    f"{event.player.name} is using {event.selected_special.name} on {event.battle.monster}"
                )
                print(
                    f"monster {event.battle.monster.name}'s health is now: {event.battle.monster.hp}"
                )
                print(f"{event.player.name}'s stats are now: {event.player}")
            case BattlePlayerPASSEvent():
                print(
                    f"{event.player.name} is passing their turn in the battle against {event.battle.monster}"
                )
                print(f"{event.player.name}'s stats are: {event.player}")
            case BattlePlayerLEAVEEvent():
                print(
                    f"{event.player.name} is leaving the battle against {event.battle.monster}"
                )
                print(f"{event.player.name}'s stats are: {event.player}")
            case PlayerStatusRequestEvent():
                print(
                    f"{event.player.name}: {event.player} at position {event.player.position}"
                )
                if event.tile.battle is not None:
                    print(
                        f"{event.player.name} is in battle against {event.tile.battle.monster}"
                    )
            case _:
                print(f"unimplemented handling for event: {event}")

    def read_command(self) -> list[str]:
        try:
            command = input("Enter action: ").split(maxsplit=2)
        except EOFError:
            print("")
            raise SystemExit
        except KeyboardInterrupt:
            print("")
            raise SystemExit
        # make sure we have at least 2 words
        # FIXME: raise a custom error instead of returning ("", [])
        if len(command) < 2:
            print("invalid input")
            raise InvalidInputError()
        else:
            return command

    def run(self):
        self.game.start()
        # TODO: loop forever (until exited, at least) and receive text input on what actions the players take.
        # since this isn't turn based, the input must be prepended with a number corresponding to the player (indexed at 0)
        while True:
            try:
                # command[0] is the player name, command[1:] is the command
                command = self.read_command()
                match command[1]:
                    case "up":
                        try:
                            self.game.move_player(command[0], Direction.UP)
                        except PlayerNotFoundError:
                            print(f"ERROR: player {command[0]} was not found!")
                        except PlayerInvalidActionError:
                            print(
                                f"ERROR: invalid action specified for player's current state."
                            )
                        except PlayerInvalidPositionError:
                            print(f"ERROR: player cannot move in that direction")
                    case "down":
                        try:
                            self.game.move_player(command[0], Direction.DOWN)
                        except PlayerNotFoundError:
                            print(f"ERROR: player {command[0]} was not found!")
                        except PlayerInvalidActionError:
                            print(
                                f"ERROR: invalid action specified for player's current state."
                            )
                        except PlayerInvalidPositionError:
                            print(f"ERROR: player cannot move in that direction")
                    case "right":
                        try:
                            self.game.move_player(command[0], Direction.RIGHT)
                        except PlayerNotFoundError:
                            print(f"ERROR: player {command[0]} was not found!")
                        except PlayerInvalidActionError:
                            print(
                                f"ERROR: invalid action specified for player's current state."
                            )
                        except PlayerInvalidPositionError:
                            print(f"ERROR: player cannot move in that direction")
                    case "left":
                        try:
                            self.game.move_player(command[0], Direction.LEFT)
                        except PlayerNotFoundError:
                            print(f"ERROR: player {command[0]} was not found!")
                        except PlayerInvalidActionError:
                            print(
                                f"ERROR: invalid action specified for player's current state."
                            )
                        except PlayerInvalidPositionError:
                            print(f"ERROR: player cannot move in that direction")
                    case "changename":
                        if len(command) < 3:
                            raise InvalidInputError()
                        else:
                            try:
                                self.game.change_player_name(command[0], command[2])
                            except PlayerNotFoundError:
                                print(f"ERROR: player {command[0]} was not found!")
                    case "chat":
                        if len(command) < 3:
                            raise InvalidInputError()
                        else:
                            try:
                                self.game.send_message(command[0], command[2])
                            except PlayerNotFoundError:
                                print(f"ERROR: player {command[0]} was not found!")
                    case "debug_givexp":
                        if len(command) < 3:
                            raise InvalidInputError()
                        else:
                            try:
                                xp_amt = int(command[2])
                            except ValueError:
                                print("expected number")
                            else:
                                self.game.debug_give_player_xp(command[0], xp_amt)
                    case "debug_createbattle":
                        self.game.debug_create_battle(
                            command[0], self.game._get_random_monster()
                        )
                    case "attack":
                        try:
                            self.game.battle_action(command[0], BattleAction.ATTACK)
                        except PlayerNotFoundError:
                            print(f"ERROR: player {command[0]} was not found!")
                        except PlayerInvalidActionError:
                            print(
                                f"ERROR: invalid action specified for player's current state."
                            )
                        except OutOfTurnError:
                            print(f"ERROR: not player {command[0]}'s turn!")
                    case "special":
                        # need to ensure command[2] exists here
                        if len(command) < 3:
                            print(f"expected name of special")
                            raise InvalidInputError()
                        else:
                            try:
                                self.game.battle_action(
                                    command[0], BattleAction.SPECIAL, command[2]
                                )
                            except SpecialNotFoundError:
                                print(f"ERROR: special {command[2]} was not found")
                            except SpecialCostsTooMuchError:
                                print(
                                    f"ERROR: special {command[2]} costs more mp than available"
                                )
                    case "pass":
                        try:
                            self.game.battle_action(command[0], BattleAction.PASS)
                        except OutOfTurnError:
                            print(f"ERROR: not player {command[0]}'s turn!")
                    case "leave":
                        self.game.battle_action(command[0], BattleAction.LEAVE)
                    case "status":
                        # TODO: print current status of the player. maybe this calls a function that generates an event with relevant info, depending on player state?
                        try:
                            self.game.player_status(command[0])
                        except PlayerNotFoundError:
                            print(f"ERROR: player {command[0]} was not found!")
                    case _:
                        print(f"unknown command(s) {command}")
            except InvalidInputError:
                print("invalid input received. not doing anything.")
