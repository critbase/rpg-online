import logging
from threading import Lock

from gamecomm.server import GameConnection
from gamedb.mongo import MongoGameRepository, MongoUserRepository
from gamedb import User

from model.Player import Player

from .controller import PlayerController
from .publisher import GamePublisher
from .ThreadSafeGame import ThreadSafeGame


logger = logging.getLogger(__name__)


class GameManager:
    def __init__(
        self,
        gid: str,
        user_repository: MongoUserRepository,
        game_repository: MongoGameRepository,
        destruct=None,
    ):
        self.gid = gid
        self.publisher = GamePublisher()
        self._controllers: dict[GameConnection, PlayerController] = {}
        self._lock = Lock()
        self._game = ThreadSafeGame(self.publisher.publish_event, 32, 32)
        self._user_repository = user_repository
        self._game_repository = game_repository
        self._destruct = destruct

    def handle_close(self, connection: GameConnection):
        with self._lock:
            # save user data before dying
            p_info: User = self._user_repository.find_user(connection.uid)
            p_info.custom = self._game.get_player_as_dict(connection.uid)
            self._user_repository.replace_user(p_info)
            # goodbye
            self._controllers.pop(connection)
            self.publisher.remove_subscriber(connection)
            self._game.remove_player(connection.uid)
            self._game_repository.games.update_one(
                {"gid": self.gid}, {"$pull": {"players": connection.uid}}
            )
            # if no more players connected, get rid of the manager
            if len(self._controllers) == 0:
                if self._destruct is not None:
                    self._destruct(self.gid)

    def handle_connection(self, connection: GameConnection):
        controller = PlayerController(
            connection, self.publisher, self._game, on_close=self.handle_close
        )
        with self._lock:
            self._controllers[connection] = controller
            self.publisher.add_subscriber(connection)
            # obtain user credentials from the mongodb user database!
            if self._user_repository:
                player_info: User = self._user_repository.find_user(connection.uid)
                if player_info.custom is not None:
                    print("adding existing player")
                    self._game.add_existing_player(
                        connection.uid, Player.from_dict(player_info.custom)
                    )
                else:
                    self._game.add_new_player(
                        connection.uid, player_name=connection.uid
                    )
            else:
                self._game.add_new_player(connection.uid, connection.uid)
            self._game_repository.games.update_one(
                {"gid": self.gid}, {"$addToSet": {"players": connection.uid}}
            )
        controller.run()

    def stop(self):
        with self._lock:
            for controller in self._controllers.values():
                controller.stop()
