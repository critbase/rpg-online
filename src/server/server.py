import logging
from threading import Lock

from gameauth import TokenValidator, InvalidTokenError
from gamecomm.server import WsGameListener
from gamedb.mongo import MongoUserRepository, MongoGameRepository

from .manager import GameManager


logger = logging.getLogger(__name__)


class RPGServer:
    def __init__(
        self,
        local_ip: str,
        local_port: int,
        token_validator: TokenValidator,
        user_repository: MongoUserRepository,
        game_repository: MongoGameRepository,
    ):
        self.local_ip = local_ip
        self.local_port = local_port
        self.token_validator = token_validator
        self._managers: dict[str, GameManager] = {}
        self._lock = Lock()

        # get user credentials and persistent data directly from the db
        self._user_repository = user_repository
        self._game_repository = game_repository

    def _find_or_create_manager(self, gid: str):
        with self._lock:
            if gid not in self._managers:
                self._managers[gid] = GameManager(
                    gid,
                    self._user_repository,
                    self._game_repository,
                    self.destroy_manager,
                )
                logger.info(f"created new manager for gid {gid}")
            return self._managers[gid]

    def handle_authentication(self, gid: str, token: str):
        try:
            return self.token_validator.validate(gid, token)
        except InvalidTokenError:
            return None

    def handle_connection(self, connection):
        manager = self._find_or_create_manager(connection.gid)
        manager.handle_connection(connection)

    def handle_stop(self):
        with self._lock:
            for manager in self._managers.values():
                manager.stop()

    def destroy_manager(self, gid: str):
        with self._lock:
            self._managers.pop(gid)
            self._game_repository.delete_game(gid)

    def run(self):
        logger.info(f"listening on {self.local_ip}:{self.local_port}")
        logger.info(
            f"authentication is {'enabled' if self.token_validator else 'disabled'}"
        )
        listener = WsGameListener(
            self.local_ip,
            self.local_port,
            on_connection=self.handle_connection,
            on_authenticate=self.handle_authentication
            if self.token_validator
            else None,
            on_stop=self.handle_stop,
        )
        listener.run()
