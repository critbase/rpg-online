import logging
from threading import Event
from typing import Callable, Any

from gamecomm.server import GameConnection, ConnectionClosedOK, ConnectionClosedError
from model.Battle import BattleAction

from model.Events import Direction
from model.Errors import *

from .publisher import GamePublisher
from .ThreadSafeGame import ThreadSafeGame


logger = logging.getLogger(__name__)

# FIXME: convert this to an enum and match on the enum in handle_command
COMMANDS = [
    "up",
    "down",
    "left",
    "right",
    "chat",
    "debug_givexp",
    "debug_createbattle",
    "attack",
    "special",
    "pass",
    "leave",
    "status",
]


class PlayerController:
    RECV_TIMEOUT_SECONDS = 0.250

    def __init__(
        self,
        connection: GameConnection,
        publisher: GamePublisher,
        game: ThreadSafeGame,
        on_close: Callable[[GameConnection], None],
    ):
        self.connection = connection
        self.publisher = publisher
        self._game = game
        self.on_close = on_close
        self._shutdown = Event()

    def handle_command(self, request: dict[str, Any]) -> dict[str, Any]:
        # players are identified by self.connection.uid
        # cmd is the command
        player_id: str = self.connection.uid
        cmd: str = request["command"]
        match cmd:
            case "up":
                try:
                    self._game.move_player(player_id, Direction.UP)
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
                except PlayerInvalidActionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"invalid action specified for player {player_id}'s current state"
                        },
                    }
                except PlayerInvalidPositionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"player {player_id} cannot move in that direction"
                        },
                    }
            case "down":
                try:
                    self._game.move_player(player_id, Direction.DOWN)
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
                except PlayerInvalidActionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"invalid action specified for player {player_id}'s current state"
                        },
                    }
                except PlayerInvalidPositionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"player {player_id} cannot move in that direction"
                        },
                    }
            case "right":
                try:
                    self._game.move_player(player_id, Direction.RIGHT)
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
                except PlayerInvalidActionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"invalid action specified for player {player_id}'s current state"
                        },
                    }
                except PlayerInvalidPositionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"player {player_id} cannot move in that direction"
                        },
                    }
            case "left":
                try:
                    self._game.move_player(player_id, Direction.LEFT)
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
                except PlayerInvalidActionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"invalid action specified for player {player_id}'s current state"
                        },
                    }
                except PlayerInvalidPositionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"player {player_id} cannot move in that direction"
                        },
                    }
            # case "changename":
            #     if len(command) < 3:
            #         raise InvalidInputError()
            #     else:
            #         try:
            #             self.game.change_player_name(command[0], command[2])
            #         except PlayerNotFoundError:
            #             print(f"ERROR: player {command[0]} was not found!")
            case "chat":
                if "message" not in request:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"player {player_id} did not send a message with their chat request"
                        },
                    }
                else:
                    try:
                        self._game.send_message(player_id, request["message"])
                    except PlayerNotFoundError:
                        return {
                            "status": "error",
                            "error": {"message": f"player {player_id} was not found"},
                        }
            case "debug_givexp":
                if "xp_amt" not in request:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"player {player_id} did not send the amount of xp"
                        },
                    }
                else:
                    if request["xp_amt"] is not int:
                        return {
                            "status": "error",
                            "error": {
                                "message": f"player {player_id} did not send an integer as xp amount"
                            },
                        }
                    else:
                        self._game.debug_give_player_xp(player_id, request["xp_amt"])
            case "debug_createbattle":
                withmonster = self._game._get_random_monster()
                self._game.debug_create_battle(player_id, withmonster)
            case "attack":
                try:
                    self._game.battle_action(player_id, BattleAction.ATTACK)
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
                except PlayerInvalidActionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"invalid action specified for player {player_id}'s current state"
                        },
                    }
                except OutOfTurnError:
                    return {
                        "status": "error",
                        "error": {"message": f"not player {player_id}'s turn!"},
                    }
            case "special":
                if "special_name" not in request:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"player {player_id}: no special specified!"
                        },
                    }
                else:
                    try:
                        self._game.battle_action(
                            player_id, BattleAction.SPECIAL, request["special_name"]
                        )
                    except SpecialNotFoundError:
                        return {
                            "status": "error",
                            "error": {
                                "message": f"player {player_id}: special {request['special']} was not found"
                            },
                        }
                    except SpecialCostsTooMuchError:
                        return {
                            "status": "error",
                            "error": {
                                "message": f"player {player_id}: special {request['special']} cost more MP than available!"
                            },
                        }
                    except OutOfTurnError:
                        return {
                            "status": "error",
                            "error": {"message": f"not player {player_id}'s turn!"},
                        }
                    except PlayerInvalidActionError:
                        return {
                            "status": "error",
                            "error": {
                                "message": f"invalid action specified for player {player_id}'s current state"
                            },
                        }
                    except PlayerNotFoundError:
                        return {
                            "status": "error",
                            "error": {"message": f"player {player_id} was not found"},
                        }
            case "pass":
                try:
                    self._game.battle_action(player_id, BattleAction.PASS)
                except OutOfTurnError:
                    return {
                        "status": "error",
                        "error": {"message": f"not player {player_id}'s turn!"},
                    }
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
                except PlayerInvalidActionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"invalid action specified for player {player_id}'s current state"
                        },
                    }
            case "leave":
                try:
                    self._game.battle_action(player_id, BattleAction.LEAVE)
                except OutOfTurnError:
                    return {
                        "status": "error",
                        "error": {"message": f"not player {player_id}'s turn!"},
                    }
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
                except PlayerInvalidActionError:
                    return {
                        "status": "error",
                        "error": {
                            "message": f"invalid action specified for player {player_id}'s current state"
                        },
                    }
            case "status":
                # TODO: print current status of the player. maybe this calls a function that generates an event with relevant info, depending on player state?
                try:
                    self._game.player_status(player_id)
                except PlayerNotFoundError:
                    return {
                        "status": "error",
                        "error": {"message": f"player {player_id} was not found"},
                    }
            case _:
                return {
                    "status": "error",
                    "error": {"message": f"unknown command {cmd}"},
                }
        return {"status": "ok"}

    def run(self):
        # TODO: replace this with calls to the game model, and use self.connection.uid to identify players (may require slight changes to game model?)
        logger.info(
            f"connected to {self.connection} for user {self.connection.uid} in game {self.connection.gid}"
        )
        try:
            while not self._shutdown.is_set():
                try:
                    request = self.connection.recv(self.RECV_TIMEOUT_SECONDS)
                    logger.info(f"received request: {request}")
                    if "command" in request:
                        request["command"] = request["command"].lower()
                        if request["command"] in COMMANDS:
                            status = self.handle_command(request)
                            self.connection.send(status)
                        else:
                            self.connection.send(
                                {
                                    "status": "error",
                                    "error": {
                                        "message": f"invalid command '{request['command']}'"
                                    },
                                }
                            )

                    else:
                        self.connection.send(
                            {
                                "status": "error",
                                "error": {"message": "must specify command"},
                            }
                        )
                except TimeoutError:
                    pass
        except ConnectionClosedOK:
            pass
        except ConnectionClosedError as err:
            logger.error(f"error communicating with client: {err}")

        self.on_close(self.connection)
        logger.info(
            f"disconnected from {self.connection} for user {self.connection.uid} in game {self.connection.gid}"
        )

    def stop(self):
        logger.info("handling stop")
        self._shutdown.set()
