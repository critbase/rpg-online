from threading import Lock
from model.Battle import BattleAction
from model.Events import Direction
from model.Game import Game, GameObserver
from model.Monster import Monster
from model.Player import Player


class ThreadSafeGame:
    def __init__(
        self, observer: GameObserver, world_width: int = 32, world_height: int = 32
    ):
        self._game = Game(observer, world_width, world_height)
        self._lock = Lock()

    # TODO: implement wrappers for all 'public' functions

    def add_existing_player(self, uid: str, player: Player):
        with self._lock:
            return self._game.add_existing_player(uid, player)

    def add_new_player(self, uid: str, player_name: str):
        with self._lock:
            return self._game.add_new_player(uid, player_name)

    def remove_player(self, uid: str):
        with self._lock:
            return self._game.remove_player(uid)

    def move_player(self, uid: str, direction: Direction):
        with self._lock:
            return self._game.move_player(uid, direction)

    def player_status(self, uid: str):
        with self._lock:
            return self._game.player_status(uid)

    def get_player_as_dict(self, uid: str):
        with self._lock:
            return self._game.get_player_as_dict(uid)

    def battle_action(self, uid: str, action: BattleAction, special_name: str = ""):
        with self._lock:
            return self._game.battle_action(uid, action, special_name)

    def send_message(self, player_name: str, message: str):
        with self._lock:
            return self._game.send_message(player_name, message)

    def debug_create_battle(self, player_name: str, monster_type: Monster):
        with self._lock:
            return self._game.debug_create_battle(player_name, monster_type)

    def debug_give_player_xp(self, player_name: str, xp_amount: int):
        with self._lock:
            return self._game.debug_give_player_xp(player_name, xp_amount)

    # FIXME: really shouldn't use this; we can just have the debug command in the server require that the monster type be specified, and look that up in a dict (which would likely be located in Monster.py)
    def _get_random_monster(self) -> Monster:
        with self._lock:
            return self._game._get_random_monster()
