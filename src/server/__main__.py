import logging
import sys
from gameauth import TokenValidator
from pymongo import MongoClient
from gamedb.mongo import MongoGameRepository, MongoUserRepository

import server.config as config
from .server import RPGServer

if __name__ == "__main__":
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format="%(levelname)s %(name)s %(threadName)s %(message)s",
    )

    enable_auth = bool(config.ENABLE_AUTH)
    token_validator = (
        TokenValidator(config.TOKEN_ISSUER_URI, config.PUBLIC_KEY_FILE)
        if enable_auth
        else None
    )

    mongo_client = MongoClient(config.MONGO_URL) if enable_auth else None
    user_repository = MongoUserRepository(mongo_client) if mongo_client else None
    game_repository = MongoGameRepository(mongo_client) if mongo_client else None

    server = RPGServer(
        config.LOCAL_IP,
        int(config.WS_LISTENER_PORT),
        token_validator,
        user_repository,
        game_repository,
    )
    server.run()
