import dataclasses
import logging
from threading import Lock

from gamecomm.server import GameConnection
from model.Events import GameEvent

logger = logging.getLogger(__name__)


class GamePublisher:
    def __init__(self):
        self._subscribers: list[GameConnection] = []
        self._lock = Lock()

    def add_subscriber(self, connection: GameConnection):
        logger.info(f"adding subscriber {connection}")
        with self._lock:
            self._subscribers.append(connection)

    def remove_subscriber(self, connection: GameConnection):
        logger.info(f"removing subscriber {connection}")
        with self._lock:
            self._subscribers.remove(connection)

    def publish_event(self, event: GameEvent):
        logger.info(f"publishing event: {event}")
        with self._lock:
            subscribers = list(self._subscribers)
        for subscriber in subscribers:
            event_dict = {"event": dataclasses.asdict(event)}
            event_dict["event"]["event_name"] = event.event_name
            subscriber.send(event_dict)
