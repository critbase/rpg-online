import argparse
import sys
import time
from urllib.parse import urlsplit, urlunsplit
import pygame

import requests
from requests.auth import HTTPBasicAuth

from .ConnectionUI import ConnectionUI
from .event_handler import MyEventHandler
from .client import MyClient
from .gui import GUI

DISPLAY_SIZE = (640, 480)
DEFAULT_URL = "ws://127.0.0.1:10020"


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--uid", help="user ID for the API server")
    parser.add_argument("-p", "--password", help="password for the API server")
    parser.add_argument(
        "-g", "--game-url", default=DEFAULT_URL, help="URL for the game"
    )
    args = parser.parse_args()
    if args.uid or args.password:
        if not args.uid or not args.password:
            print("must both -u and -p", file=sys.stderr)
            exit(1)
    return args


def fetch_game(uid: str, password: str, game_url: str) -> dict:
    auth = HTTPBasicAuth(uid, password)
    response = requests.get(game_url, auth=auth)
    response.raise_for_status()
    return response.json()


def fetch_player_name(uid: str, game_url: str) -> str:
    # the game_url gives us the base URL for the API; we just need to reassemble it into a user URL
    url_parts = urlsplit(game_url)
    user_path = f"/users/{uid}"
    user_url = urlunsplit(
        (
            url_parts.scheme,
            url_parts.netloc,
            user_path,
            url_parts.query,
            url_parts.fragment,
        )
    )

    # now fetch the user
    response = requests.get(user_url)
    response.raise_for_status()

    user = response.json()
    # we prefer the nickname, but we'll take either the full name, or the just the user ID instead
    return user.get("nickname", user.get("full_name", uid))


if __name__ == "__main__":
    # args = parse_args()

    # if the user ID (and password) is specified, assume that the URL is for the games API
    # and fetch the details for the game and user from the API. Otherwise, assume that the
    # URL is for the game server and it is running with authentication disabled
    # if args.uid:
    #    game = fetch_game(args.uid, args.password, args.game_url)
    #    player_name = fetch_player_name(args.uid, args.game_url)
    #    server_url = game["server"]
    #    token = game["token"]
    # else:
    #    server_url = args.game_url
    #    token = None
    #    player_name = None

    event_handler = MyEventHandler()
    # initialize a pygame surface to use and reuse
    pygame.init()
    window = pygame.display.set_mode(DISPLAY_SIZE)
    connection_ui = ConnectionUI(window)
    conn_details = connection_ui.run()
    # verify we got everything we needed
    if conn_details is None:
        print("Exiting early!")
        exit()

    # TODO: some form of loading screen or image
    window.fill("black")
    pygame.display.flip()

    # get the game from the server
    game = fetch_game(
        conn_details["username"], conn_details["password"], conn_details["game_url"]
    )
    # get the player name from the server
    player_name = fetch_player_name(conn_details["username"], conn_details["game_url"])
    # extract necessary details
    server_url = game["server"]
    token = game["token"]
    # start the client that listens to server events
    client = MyClient(url=server_url, token=token, on_event=event_handler.handle_event)
    client.start()
    # start the actual GUI for the game
    gui = GUI(window, client, event_handler, player_name)
    try:
        gui.run()
    except KeyboardInterrupt:
        pass

    client.stop()
