import pygame


def normal(size: int = 24):
    return pygame.font.SysFont("Arial", size)


def monospace(size: int = 20):
    return pygame.font.SysFont(["Monaco", "Courier New"], size)


def pixel(size: int = 27):
    return pygame.font.Font(
        "src/gui/custom_fonts/cc_redalert_inet/C&C Red Alert [INET].ttf", size
    )
