from typing import Callable
import pygame

from .client import GameClient


class KeyboardCommands:
    def __init__(self, on_key_press: Callable[[str], None], client: GameClient = None):
        self.on_key_press = on_key_press
        self.client = client
        self._pending_commands: list[str] = []

    def handle_success(self, response: dict):
        command = self._pending_commands.pop(0)
        print(f"ok: command '{command}' completed successfully")

    def handle_error(self, response: dict):
        command = self._pending_commands.pop(0)
        message = response["error"].get("message", "unknown error")
        print(f"error: command '{command}' failed: {message}")

    def consume_event(self, event: pygame.event.Event):
        if event.key == pygame.K_DOWN:
            print("down")
            self._pending_commands.append("down")
            self.client.send(
                {"command": "down"},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
            self.on_key_press("down")
        elif event.key == pygame.K_UP:
            print("up")
            self._pending_commands.append("up")
            self.client.send(
                {"command": "up"},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
            self.on_key_press("up")
        elif event.key == pygame.K_LEFT:
            print("left")
            self._pending_commands.append("left")
            self.client.send(
                {"command": "left"},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
            self.on_key_press("left")
        elif event.key == pygame.K_RIGHT:
            print("right")
            self._pending_commands.append("right")
            self.client.send(
                {"command": "right"},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
            self.on_key_press("right")
