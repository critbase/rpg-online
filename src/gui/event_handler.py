from queue import Queue, Empty


class MyEventHandler:

    def __init__(self):
        self._queue = Queue()

    def handle_event(self, event):
        self._queue.put(event)

    def pending_events(self) -> list[dict]:
        events = []
        try:
            while True:
                events.append(self._queue.get(block=False))
        except Empty:
            pass

        return events
