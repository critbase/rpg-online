from ast import literal_eval
from typing import Any
import pygame
import pygame_gui


from .client import MyClient
from .event_handler import MyEventHandler
from .event_view import EventView
from .MapView import MapView
from .SpriteView import SpriteView
from .BattleMenu import BattleMenu
from .KeyboardCommands import KeyboardCommands


DISPLAY_SIZE = (640, 480)
FRAME_RATE = 30
WINDOW_BG_COLOR = "lightgray"
WINDOW_FG_COLOR = "darkgreen"
WINDOW_TITLE = "RPG"


class GUI:
    def __init__(
        self,
        window: pygame.Surface,
        client: MyClient,
        event_handler: MyEventHandler,
        player_name: str,
    ):
        self.window = window
        self.client = client
        self.event_handler = event_handler
        self.player_name = player_name
        self.player_pos: tuple[int, int] = None
        self.player_level: int = 1
        self.in_battle: bool = False

        self._pending_commands: list[str] = list()

    def handle_success(self, response: dict):
        command = self._pending_commands.pop(0)
        print(f"ok: command '{command}' completed successfully")

    def handle_error(self, response: dict):
        command = self._pending_commands.pop(0)
        message = response["error"].get("message", "unknown error")
        print(f"error: command '{command}' failed: {message}")

    def run(self):
        pygame.init()

        # self.window = pygame.display.set_mode(DISPLAY_SIZE)
        manager = pygame_gui.UIManager(DISPLAY_SIZE)

        title = (
            f"{self.player_name} @ {WINDOW_TITLE}" if self.player_name else WINDOW_TITLE
        )
        pygame.display.set_caption(title)

        # display the sprite
        self.map_view = MapView(self.window)
        self.sprite_view = SpriteView(self.window, 1, 1)
        self.battle_menu = BattleMenu(
            self.window, DISPLAY_SIZE, self.sprite_view, client=self.client
        )
        self.battle_menu.player_label.set_text(self.player_name)
        compass = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((10, 10), (-1, 30)),
            plain_text_display_only=True,
            html_text="",
            manager=manager,
        )
        level_display = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((10, 40), (-1, 30)),
            plain_text_display_only=True,
            html_text="",
            manager=manager,
        )
        chat_input = pygame_gui.elements.UITextEntryLine(
            relative_rect=pygame.Rect((120 - 50 - 50, 450), (600, 30)),
            placeholder_text="Enter message...",
            manager=manager,
        )
        self.chat_log = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((120 - 50 - 50, 350), (600, 100)),
            html_text="",
            manager=manager,
        )
        # event_view = EventView(window, color=WINDOW_FG_COLOR)
        keyboard_commands = KeyboardCommands(
            on_key_press=self.on_key_press, client=self.client
        )

        done = False
        clock = pygame.time.Clock()
        while not done:
            for pending_event in self.event_handler.pending_events():
                # Handle events received from the game here.
                # event_view.append(pending_event["event"])
                self.update_state_from_events(pending_event)

            compass.set_text(f"{self.player_pos}")
            level_display.set_text(f"Level: {self.player_level}")

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                elif event.type == pygame.KEYDOWN:
                    if not chat_input.is_focused:
                        print("key press")
                        keyboard_commands.consume_event(event)
                elif event.type == pygame_gui.UI_BUTTON_PRESSED:
                    # check for the ui elements we define, if any
                    if self.in_battle:
                        self.battle_menu.consume_ui_event(event)
                elif event.type == pygame_gui.UI_SELECTION_LIST_NEW_SELECTION:
                    if self.in_battle:
                        self.battle_menu.consume_ui_event(event)
                elif event.type == pygame_gui.UI_TEXT_ENTRY_FINISHED:
                    if event.ui_element == chat_input:
                        self._pending_commands.append("chat")
                        self.client.send(
                            {"command": "chat", "message": chat_input.text},
                            on_success=self.handle_success,
                            on_error=self.handle_error,
                        )
                        chat_input.clear()
                        chat_input.focus()
                if self.in_battle:
                    self.battle_menu.ui_manager.process_events(event)
                else:
                    manager.process_events(event)

            self.battle_menu.ui_manager.update(FRAME_RATE)
            manager.update(FRAME_RATE)

            self.window.fill(WINDOW_BG_COLOR)
            # event_view.draw()
            self.map_view.draw(self.player_pos)
            self.sprite_view.draw(player_name=self.player_name)
            if self.in_battle:
                self.battle_menu.draw()
            else:
                manager.draw_ui(self.window)
            pygame.display.flip()
            clock.tick(FRAME_RATE)

        pygame.quit()

    def update_state_from_events(self, pending_event: dict[str, Any]):
        match pending_event["event"]["event_name"]:
            case "GameStartedEvent":
                # we are never going to see this
                pass
            case "PlayerMoveEvent":
                # change player location if event is current player
                if pending_event["event"]["player_name"] == self.player_name:
                    self.player_pos = (
                        pending_event["event"]["newposition"][0],
                        pending_event["event"]["newposition"][1],
                    )
                # else show other players updated locations on current players map
                else:
                    pos: tuple[int, int, str] = (
                        pending_event["event"]["newposition"][0],
                        pending_event["event"]["newposition"][1],
                        pending_event["event"]["direction"],
                    )
                    self.map_view.players[pending_event["event"]["player_name"]] = pos
            case "PlayerJoinBattleEvent":
                if pending_event["event"]["player_name"] == self.player_name:
                    self.in_battle = True
                    self.sprite_view.current_health = pending_event["event"][
                        "player_hp"
                    ][0]
                    self.sprite_view.health_capacity = pending_event["event"][
                        "player_hp"
                    ][1]

                    self.sprite_view.mp.current_health = pending_event["event"][
                        "player_mp"
                    ][0]
                    self.sprite_view.mp.health_capacity = pending_event["event"][
                        "player_mp"
                    ][1]
                    self.battle_menu.player_mp.rebuild()

                battle_pos = (
                    pending_event["event"]["position"][0],
                    pending_event["event"]["position"][1],
                )

                if (battle_pos == self.player_pos) and self.in_battle:
                    self.battle_menu.battle_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['player_name']} has joined the battle against {pending_event['event']['monster']['name']}</font>\n"
                    )
                    self.battle_menu.set_monster(
                        pending_event["event"]["monster"]["name"]
                    )
                    self.battle_menu.monster_sprite.current_health = pending_event[
                        "event"
                    ]["monster"]["hp"]
                    self.battle_menu.monster_sprite.health_capacity = pending_event[
                        "event"
                    ]["monster"]["max_hp"]
                    self.battle_menu.monster_health.rebuild()
                else:
                    self.chat_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['player_name']} has joined the battle on tile {pending_event['event']['position']} against {pending_event['event']['monster']['name']}</font>\n"
                    )
            case "BattleCreatedEvent":
                self.chat_log.append_html_text(
                    f"<font color='#FFC300'>{pending_event['event']['player_name']} has found a battle on tile {pending_event['event']['newposition']} against {pending_event['event']['monster']['name']}</font>\n"
                )
            case "PlayerJoinEvent":
                # add current player
                if self.player_name == pending_event["event"]["player_name"]:
                    self.parse_world_dict(pending_event["event"]["world"])
                    # apparently tuples are serialized to lists/arrays?
                    self.player_pos = (
                        pending_event["event"]["player_pos"][0],
                        pending_event["event"]["player_pos"][1],
                    )
                    self.player_level = pending_event["event"]["player_level"]
                    self.sprite_view.current_health = pending_event["event"][
                        "player_hp"
                    ][0]
                    self.sprite_view.health_capacity = pending_event["event"][
                        "player_hp"
                    ][1]
                    self.battle_menu.special_list.set_item_list(
                        pending_event["event"]["specials"]
                    )
                    # we're joining, so populate our map view (potentially)
                    other_players = pending_event["event"]["other_players"]
                    for player in other_players:
                        if player != self.player_name:
                            self.map_view.players[player] = (
                                other_players[player][0],
                                other_players[player][1],
                                "DOWN",
                            )
                # else add other player to world
                else:
                    self.chat_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['player_name']} has joined!</font>\n"
                    )
                    pos: tuple[int, int, str] = (
                        pending_event["event"]["player_pos"][0],
                        pending_event["event"]["player_pos"][1],
                        "DOWN",
                    )
                    self.map_view.players[pending_event["event"]["player_name"]] = pos
            case "PlayerLeaveEvent":
                self.chat_log.append_html_text(
                    f"<font color='#FFC300'>{pending_event['event']['player_name']} has left!</font>\n"
                )
                # remove from MapView list of players
                if pending_event["event"]["player_name"] in self.map_view.players:
                    self.map_view.players.pop(pending_event["event"]["player_name"])
            case "PlayerNicknameEvent":
                # currently unused
                pass
            case "PlayerChatEvent":
                self.chat_log.append_html_text(
                    f"{pending_event['event']['player_name']}: {pending_event['event']['message']}\n"
                )
            case "PlayerLevelUpEvent":
                if self.player_name == pending_event["event"]["player_name"]:
                    self.player_level = pending_event["event"]["level"]
            case "PlayerGainSkillEvent":
                if self.player_name == pending_event["event"]["player_name"]:
                    self.battle_menu.special_list.add_items(
                        [pending_event["event"]["skill_name"]]
                    )
            case "BattleWonEvent":
                if self.player_name in pending_event["event"]["players"]:
                    # TODO: display a pop up box or something when a battle is won
                    self.battle_menu.battle_log.clear()
                    self.in_battle = False
                    self.chat_log.append_html_text(
                        f"<font color='#FFC300'>You have won the battle!</font>\n"
                    )
                else:
                    self.chat_log.append_html_text(
                        f"<font color='#FFC300'>Players {pending_event['event']['players']} have won the battle on tile {pending_event['event']['position']}!</font>\n"
                    )
            case "BattleLostEvent":
                if self.player_name in pending_event["event"]["players"]:
                    # TODO: display a pop up box or something when a battle is lost
                    self.battle_menu.battle_log.clear()
                    self.in_battle = False
                    self.chat_log.append_html_text(
                        f"<font color='#FFC300'>You have lost the battle.</font>\n"
                    )
                else:
                    if len(pending_event["event"]["players"]) > 0:
                        self.chat_log.append_html_text(
                            f"<font color='#FFC300'>Players {pending_event['event']['players']} have lost the battle on tile {pending_event['event']['position']}</font>\n"
                        )
                    else:
                        self.chat_log.append_html_text(
                            f"<font color='#FFC300'>The battle on tile {pending_event['event']['position']} has been abandoned.</font>\n"
                        )
            case "BattleMonsterAttackEvent":
                battle_pos = (
                    pending_event["event"]["position"][0],
                    pending_event["event"]["position"][1],
                )
                if self.in_battle and (self.player_pos == battle_pos):
                    self.battle_menu.battle_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['monster']['name']} has dealt {pending_event['event']['attack_damage']} damage to {pending_event['event']['target_player']}</font>\n"
                    )
                    if self.player_name == pending_event["event"]["target_player"]:
                        self.sprite_view.current_health = pending_event["event"][
                            "target_player_hp"
                        ]
                    self.battle_menu.monster_sprite.current_health = pending_event[
                        "event"
                    ]["monster"]["hp"]
                    self.battle_menu.monster_sprite.health_capacity = pending_event[
                        "event"
                    ]["monster"]["max_hp"]
                    self.battle_menu.monster_health.rebuild()
            case "BattlePlayerATTACKEvent":
                battle_pos = (
                    pending_event["event"]["position"][0],
                    pending_event["event"]["position"][1],
                )
                if self.in_battle and (self.player_pos == battle_pos):
                    self.battle_menu.battle_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['player']} has dealt {pending_event['event']['damage']} damage</font>\n"
                    )
                    self.battle_menu.monster_sprite.current_health = pending_event[
                        "event"
                    ]["monster"]["hp"]
                    self.battle_menu.monster_sprite.health_capacity = pending_event[
                        "event"
                    ]["monster"]["max_hp"]
                    self.battle_menu.monster_health.rebuild()

            case "BattlePlayerSPECIALEvent":
                battle_pos = (
                    pending_event["event"]["position"][0],
                    pending_event["event"]["position"][1],
                )
                if self.in_battle and (self.player_pos == battle_pos):
                    self.battle_menu.battle_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['player']} used {pending_event['event']['selected_special']['name']}</font>\n"
                    )
                    self.battle_menu.monster_sprite.current_health = pending_event[
                        "event"
                    ]["monster"]["hp"]
                    self.battle_menu.monster_sprite.health_capacity = pending_event[
                        "event"
                    ]["monster"]["max_hp"]
                    self.battle_menu.monster_health.rebuild()

                if self.player_name == pending_event["event"]["player"]:
                    self.sprite_view.mp.current_health = pending_event["event"][
                        "player_mp"
                    ][0]
                    self.sprite_view.mp.health_capacity = pending_event["event"][
                        "player_mp"
                    ][1]
            case "BattlePlayerPASSEvent":
                battle_pos = (
                    pending_event["event"]["position"][0],
                    pending_event["event"]["position"][1],
                )
                if self.in_battle and (self.player_pos == battle_pos):
                    self.battle_menu.battle_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['player']} passed their turn</font>\n"
                    )
                    self.battle_menu.monster_sprite.current_health = pending_event[
                        "event"
                    ]["monster"]["hp"]
                    self.battle_menu.monster_sprite.health_capacity = pending_event[
                        "event"
                    ]["monster"]["max_hp"]
                    self.battle_menu.monster_health.rebuild()

            case "BattlePlayerLEAVEEvent":
                battle_pos = (
                    pending_event["event"]["position"][0],
                    pending_event["event"]["position"][1],
                )
                if self.in_battle and (self.player_pos == battle_pos):
                    self.battle_menu.battle_log.append_html_text(
                        f"<font color='#FFC300'>{pending_event['event']['player']} has left the battle</font>\n"
                    )

                if pending_event["event"]["player"] == self.player_name:
                    self.battle_menu.battle_log.clear()
                    self.in_battle = False
            case "BattlePlayerDiedEvent":
                if self.player_name == pending_event["event"]["player"]:
                    self.sprite_view.current_health = 1
                    self.battle_menu.battle_log.clear()
                    self.in_battle = False
            case "BattleTurnAdvancedEvent":
                battle_pos = (
                    pending_event["event"]["position"][0],
                    pending_event["event"]["position"][1],
                )
                if self.in_battle and (self.player_pos == battle_pos):
                    self.battle_menu.update_turn_list(pending_event)
            case _:
                pass

    def parse_world_dict(self, world_dict: dict[str, Any]):
        for pos_str in world_dict:
            pos: tuple[int, int] = literal_eval(pos_str)
            self.map_view.world[pos] = world_dict[pos_str]

    def on_key_press(self, direction: str):
        self.sprite_view.facedir(direction)
