import pygame
import gui.fonts as font

HeroSprite = {
    "up": ["src/gui/sprites/hero/up00.png", "src/gui/sprites/hero/up01.png"],
    "down": ["src/gui/sprites/hero/down00.png", "src/gui/sprites/hero/down01.png"],
    "left": ["src/gui/sprites/hero/left00.png", "src/gui/sprites/hero/left01.png"],
    "right": ["src/gui/sprites/hero/right00.png", "src/gui/sprites/hero/right01.png"],
}


class MPContainer(pygame.sprite.Sprite):
    def __init__(self):
        self.current_health = 1
        self.health_capacity = 1


class SpriteView(pygame.sprite.Sprite):
    def __init__(self, surface: pygame.Surface, max_hp: int, hp: int, *groups):
        super().__init__(*groups)
        self.current_health = hp
        self.health_capacity = max_hp

        self.surface = surface
        self.sprite = self._loadsprite(HeroSprite["down"][0])
        self.rect = self.sprite.get_rect().move(
            self.surface.get_rect().center[0] - (16 * 2),
            self.surface.get_rect().center[1] - (16 * 2) - 60,
        )

        self.color = "white"
        self.bgcolor = "black"
        self._font = font.pixel()

        self.mp = MPContainer()

    def _loadsprite(self, path: str) -> pygame.Surface:
        loaded_img = pygame.image.load(path)
        return pygame.transform.scale(loaded_img, (16 * 4, 16 * 4))

    def facedir(self, direction: str):
        match direction:
            case "up":
                self.sprite = self._loadsprite(HeroSprite["up"][0])
            case "down":
                self.sprite = self._loadsprite(HeroSprite["down"][0])
            case "left":
                self.sprite = self._loadsprite(HeroSprite["left"][0])
            case "right":
                self.sprite = self._loadsprite(HeroSprite["right"][0])
            case _:
                pass

    def draw(self, player_name: str):
        ptext = self._font.render(player_name, True, self.color)
        text_bg = pygame.Surface(ptext.get_size())
        text_bg.fill(self.bgcolor)
        text_bg.blit(ptext, (0, 0))

        self.surface.blit(self.sprite, self.rect)
        self.surface.blit(text_bg, self.rect.move(0, -ptext.get_height()))
