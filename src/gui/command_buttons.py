import pygame

from .client import GameClient
from .button import Button, ButtonGroup, COLOR_PRIMARY


class CommandButtons:
    def __init__(self, surface: pygame.Surface, client: GameClient = None):
        self.client = client

        self._button_group = ButtonGroup(surface, (0, 0), on_click=self.handle_click)
        self._button_group.add("leave", "Leave", COLOR_PRIMARY)
        self._button_group.add("yo", "Yo!", COLOR_PRIMARY)
        self._button_group.build()

        window_width = surface.get_width()
        window_height = surface.get_height()
        self._button_group.xy = (
            (window_width - self._button_group.get_width()) / 2,
            (window_height - self._button_group.get_height()) - 5,
        )
        self._pending_commands: list[str] = []

    def handle_success(self, response: dict):
        command = self._pending_commands.pop(0)
        print(f"ok: command '{command}' completed successfully")

    def handle_error(self, response: dict):
        command = self._pending_commands.pop(0)
        message = response["error"].get("message", "unknown error")
        print(f"error: command '{command}' failed: {message}")

    def handle_click(self, button: Button):
        if self.client:
            self._pending_commands.append(button.id)
            self.client.send(
                {"command": button.id},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )

    def consume_ui_event(self, event):
        if event.type in {
            pygame.MOUSEBUTTONUP,
            pygame.MOUSEBUTTONDOWN,
            pygame.MOUSEMOTION,
        }:
            return self._button_group.consume_ui_event(event)

    def draw(self):
        self._button_group.draw()

    def get_width(self):
        return self._button_group.get_width()

    def get_height(self):
        return self._button_group.get_height()
