from typing import Any
import pygame
import pygame_gui
import requests

from .games_api_client import GamesApiClient
from .users_api_client import UsersApiClient


DISPLAY_SIZE = (640, 480)
FRAME_RATE = 30

WINDOW_BG_COLOR = "darkgrey"


class ConnectionUI:
    def __init__(self, window: pygame.Surface):
        self.window = window
        self.api_url = ""
        self._users_api = UsersApiClient("")
        self._games_api = GamesApiClient("")

    def run(self) -> dict[str, Any]:
        manager = pygame_gui.UIManager(DISPLAY_SIZE)
        pygame.display.set_caption("Set up connection")

        username_input = pygame_gui.elements.UITextEntryLine(
            relative_rect=pygame.Rect((100, 240 - 15 - 30 - 30), (640 - 200, 30)),
            placeholder_text="Enter username",
            manager=manager,
        )
        password_input = pygame_gui.elements.UITextEntryLine(
            relative_rect=pygame.Rect((100, 240 - 15 - 30), (640 - 200, 30)),
            placeholder_text="Enter password",
            manager=manager,
        )
        server_url_input = pygame_gui.elements.UITextEntryLine(
            relative_rect=pygame.Rect((100, 240 - 15), (640 - 200, 30)),
            placeholder_text="Enter server url...",
            initial_text="https://rpg.critbase.net",
            manager=manager,
        )
        server_url_ok_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((320 - 25, 240 - 15 + 30), (50, 50)),
            text="OK",
            manager=manager,
        )

        game_list_selection = pygame_gui.elements.UISelectionList(
            relative_rect=pygame.Rect((10, 10), (300, 460)),
            item_list=[],
            manager=manager,
            visible=0,
        )
        game_list_refresh_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((320, 10), (100, 50)),
            text="Refresh",
            manager=manager,
            visible=0,
        )
        connect_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((320, 10 + 50), (100, 50)),
            text="Connect",
            manager=manager,
            visible=0,
        )
        new_game_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((320, 10 + 50 + 50), (100, 50)),
            text="New Room",
            manager=manager,
            visible=0,
        )

        done = False
        conn_details = {}
        clock = pygame.time.Clock()
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                elif event.type == pygame_gui.UI_BUTTON_PRESSED:
                    if event.ui_element == server_url_ok_button:
                        # get the server url, then try to connect
                        self.api_url = server_url_input.text
                        self._users_api.base_url = self.api_url
                        self._games_api.base_url = self.api_url
                        try:
                            user = self._find_or_create_user(
                                username_input.text, password_input.text
                            )
                            self._list_games(username_input.text, password_input.text)
                        except Exception as e:
                            user = None
                            print(f"got error {e}")
                        if user is not None:
                            server_url_input.hide()
                            server_url_ok_button.hide()
                            username_input.hide()
                            password_input.hide()
                            conn_details["username"] = username_input.text
                            conn_details["password"] = password_input.text

                            gamelist = self._list_games(
                                username_input.text, password_input.text
                            )["games"]
                            game_list_selection.set_item_list(gamelist)
                            game_list_selection.show()
                            game_list_refresh_button.show()
                            connect_button.show()
                            new_game_button.show()
                        else:
                            print("Invalid URL or credentials! Try again.")
                    elif event.ui_element == connect_button:
                        if game_list_selection.get_single_selection() is not None:
                            return conn_details
                    elif event.ui_element == game_list_refresh_button:
                        gamelist = self._list_games(
                            username_input.text, password_input.text
                        )["games"]
                        game_list_selection.set_item_list(gamelist)
                    elif event.ui_element == new_game_button:
                        # fill in conn_details
                        conn_details["game_url"] = self._create_new_game(
                            username_input.text, password_input.text
                        )
                        return conn_details
                elif event.type == pygame_gui.UI_SELECTION_LIST_NEW_SELECTION:
                    conn_details["game_url"] = "".join(
                        [
                            self.api_url,
                            f"/games/{game_list_selection.get_single_selection()}",
                        ]
                    )
                manager.process_events(event)

            manager.update(FRAME_RATE)
            self.window.fill(WINDOW_BG_COLOR)
            manager.draw_ui(self.window)
            pygame.display.flip()
            clock.tick(FRAME_RATE)

        return None

    def _create_new_game(self, username: str, password: str):
        return "".join(
            [
                self.api_url,
                f"/games/{self._games_api.create_game([username])['gid']}",
            ]
        )

    def _list_games(self, username: str, password: str):
        self._find_or_create_user(username, password)
        self._games_api.auth(username, password)
        return self._games_api.list_games()

    def _find_or_create_user(self, username: str, password: str):
        user = self._users_api.fetch_user(username)
        if not user:
            user = self._users_api.create_user(username, password, username)
        return user
