import pygame
import gui.fonts as font
from .SpriteView import HeroSprite

Tiles = {
    "grass": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/tiles/tile_grass.png"), (16 * 4, 16 * 4)
    ),
    "water": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/tiles/tile_water.png"), (16 * 4, 16 * 4)
    ),
}


class MapView:
    def __init__(self, surface: pygame.Surface):
        self.surface = surface
        self.world: dict[tuple[int, int], bool] = dict()
        self.render_rects: list[pygame.Rect] = self._gen_rect_list()
        self.players: dict[str, tuple[int, int, str]] = dict()

    def _gen_rect_list(self):
        step: int = 16 * 4
        center_rect = (
            Tiles["grass"]
            .get_rect()
            .move(
                self.surface.get_rect().center[0] - (16 * 2),
                self.surface.get_rect().center[1] - (16 * 2) - (60),
            )
        )

        render_rects: list[pygame.Rect] = list()
        # -2, -1, 0, 1, 2
        # range(-2, 6) but in reverse
        for i in [2, 1, 0, -1, -2]:
            render_rects.append(center_rect.move(-step - step, i * step))
            render_rects.append(center_rect.move(-step, i * step))
            render_rects.append(center_rect.move(0, i * step))
            render_rects.append(center_rect.move(step, i * step))
            render_rects.append(center_rect.move(step + step, i * step))

        return render_rects

    def draw(self, currentpos: tuple[int, int]):
        if currentpos is None:
            return
        poslist: list[tuple[int, int]] = list()
        step: int = 1

        for i in [-2, -1, 0, 1, 2]:
            poslist.append((currentpos[0] - step - step, currentpos[1] + i))
            poslist.append((currentpos[0] - step, currentpos[1] + i))
            poslist.append((currentpos[0], currentpos[1] + i))
            poslist.append((currentpos[0] + step, currentpos[1] + i))
            poslist.append((currentpos[0] + step + step, currentpos[1] + i))

        for i in range(len(poslist)):
            pos = poslist[i]
            if pos in self.world:
                if self.world[pos]:
                    self.surface.blit(Tiles["grass"], self.render_rects[i])
                else:
                    self.surface.blit(Tiles["water"], self.render_rects[i])
            else:
                self.surface.blit(Tiles["water"], self.render_rects[i])

        for player in self.players:
            pos = (self.players[player][0], self.players[player][1])
            if pos in poslist:
                self.draw_other_player(
                    player,
                    self.render_rects[poslist.index(pos)],
                    self.players[player][2],
                )

    def draw_other_player(self, player_name: str, rect: pygame.Rect, direction: str):
        color = "white"
        bgcolor = "black"
        _font = font.pixel()
        loaded_img = pygame.transform.scale(
            pygame.image.load(HeroSprite[direction.lower()][0]), (16 * 4, 16 * 4)
        )
        ptext = _font.render(player_name, True, color)
        text_bg = pygame.Surface(ptext.get_size())
        text_bg.fill(bgcolor)
        text_bg.blit(ptext, (0, 0))

        self.surface.blit(loaded_img, rect)
        self.surface.blit(text_bg, rect.move(0, -ptext.get_height()))
