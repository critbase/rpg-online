import pygame
import pygame_gui
import gui.fonts as font
from gui.SpriteView import SpriteView
from .client import GameClient

Backgrounds = {
    1: pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/bg-1.png"), (128 * 3, 112 * 3)
    )
}

# TODO: different sprites for different monster types
Monsters = {
    "Slime": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/slime.png"), (21 * 4, 21 * 4)
    ),
    "Large Slime": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/slime.png"), (21 * 8, 21 * 8)
    ),
    "Queen Slime": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/redslime.png"), (17 * 4, 15 * 4)
    ),
    "Golem": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/golem.png"), (52 * 4, 56 * 4)
    ),
    "Skeleton": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/skeleton.png"), (38 * 4, 48 * 4)
    ),
    "Skeleton Guard": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/guard.png"), (48 * 4, 51 * 4)
    ),
    "Skeleton Knight": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/knight.png"), (39 * 4, 48 * 4)
    ),
    "Skeleton Ranger": pygame.transform.scale(
        pygame.image.load("src/gui/sprites/monsters/ranger.png"), (48 * 4, 51 * 4)
    ),
}

# TODO:
# - chat log and input box. can use the action log for this (copy from gui.py, just need to adjust size/position)


class MonsterView(pygame.sprite.Sprite):
    def __init__(
        self,
        surface: pygame.Surface,
        monster_img: pygame.Surface,
        max_hp: int,
        hp: int,
        *groups,
    ):
        super().__init__(*groups)
        self.current_health = hp
        self.health_capacity = max_hp

        self.monster_img = monster_img

        self.surface = surface

    def draw(self):
        center_rect = self.surface.get_rect()
        rect = center_rect.move(
            center_rect.center[0] - (self.monster_img.get_width() / 2),
            center_rect.center[1] - (self.monster_img.get_height() / 2),
        )
        self.surface.blit(self.monster_img, rect)


class BattleMenu:
    def __init__(
        self,
        surface: pygame.Surface,
        display_size: tuple[int, int],
        player_sprite: SpriteView,
        client: GameClient = None,
    ):
        self.surface = surface
        self.client = client
        self._pending_commands: list[str] = list()
        self._font_color = "white"
        self._font = font.pixel()
        self.ui_manager = pygame_gui.UIManager(display_size)

        self.monster_sprite = MonsterView(surface, Monsters["Slime"], 1, 1)

        self.player_label = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((25, 225 - 17 - 30 - 30 - 30), (100, 30)),
            plain_text_display_only=True,
            html_text="",
            manager=self.ui_manager,
        )
        self.player_health = pygame_gui.elements.UIScreenSpaceHealthBar(
            relative_rect=pygame.Rect((25, 225 - 17 - 30 - 30), (100, 30)),
            manager=self.ui_manager,
            sprite_to_monitor=player_sprite,
        )
        self.player_mp = pygame_gui.elements.UIScreenSpaceHealthBar(
            relative_rect=pygame.Rect((25, 225 - 17 - 30), (100, 30)),
            manager=self.ui_manager,
            sprite_to_monitor=player_sprite.mp,
        )
        self.player_mp.bar_filled_colour = pygame.color.Color(
            pygame.colordict.THECOLORS["blue"]
        )

        self.monster_label = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((512, 225 - 17 - 30 - 30 - 30), (120, 60)),
            plain_text_display_only=True,
            html_text="",
            manager=self.ui_manager,
        )
        self.monster_health = pygame_gui.elements.UIScreenSpaceHealthBar(
            relative_rect=pygame.Rect((512, 225 - 17 - 30), (120, 30)),
            manager=self.ui_manager,
            sprite_to_monitor=self.monster_sprite,
        )
        self.turn_display = pygame_gui.elements.UISelectionList(
            relative_rect=pygame.Rect((512, 225 - 17), (120, 272)),
            item_list=["-> test", "test2"],
            manager=self.ui_manager,
        )

        self.attack_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((25, 225 - 17), (100, 50)),
            text="attack",
            manager=self.ui_manager,
        )
        self.special_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((25, 275 - 17), (100, 50)),
            text="special",
            manager=self.ui_manager,
        )
        self.special_list = pygame_gui.elements.UISelectionList(
            relative_rect=pygame.Rect((125, 275 - 17), (150, 150)),
            item_list=[],
            manager=self.ui_manager,
            visible=0,
        )
        self.pass_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((25, 325 - 17), (100, 50)),
            text="pass",
            manager=self.ui_manager,
        )
        self.leave_button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((25, 375 - 17), (100, 50)),
            text="leave",
            manager=self.ui_manager,
        )
        self.battle_log = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((128, 375 - 17 + 50), (384, 100)),
            html_text="",
            manager=self.ui_manager,
        )

    def handle_success(self, response: dict):
        command = self._pending_commands.pop(0)
        print(f"ok: command '{command}' completed successfully")

    def handle_error(self, response: dict):
        command = self._pending_commands.pop(0)
        message = response["error"].get("message", "unknown error")
        print(f"error: command '{command}' failed: {message}")

    def consume_ui_event(self, event: pygame.event.Event):
        if event.ui_element == self.leave_button:
            self._pending_commands.append("leave")
            self.client.send(
                {"command": "leave"},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
        elif event.ui_element == self.attack_button:
            self._pending_commands.append("attack")
            self.client.send(
                {"command": "attack"},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
        elif event.ui_element == self.pass_button:
            self._pending_commands.append("pass")
            self.client.send(
                {"command": "pass"},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
        elif event.ui_element == self.special_button:
            # TODO: implement some sort of menu to select special button
            if self.special_list.visible == 1:
                self.special_list.hide()
            else:
                self.special_list.show()
            print("SPECIALS", self.special_list.item_list)
        elif event.ui_element == self.special_list:
            self._pending_commands.append("special")
            self.client.send(
                {"command": "special", "special_name": event.text},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )
            self.special_list.hide()
        self.ui_manager.process_events(event)

    def set_monster(self, monster_name: str):
        self.monster_label.set_text(monster_name)
        if monster_name in Monsters:
            self.monster_sprite.monster_img = Monsters[monster_name]

    def draw(self):
        center_rect = self.surface.get_rect()
        background_rect = center_rect.move(
            center_rect.center[0] - ((128 * 3) / 2),
            center_rect.center[1] - ((112 * 3) / 2),
        )

        # draw background
        self.surface.blit(Backgrounds[1], background_rect)
        # draw monster
        self.monster_sprite.draw()
        # draw buttons
        self.ui_manager.draw_ui(self.surface)

    def update_turn_list(self, event):
        # FIXME: probably need to check that we are getting the right event types here...
        turnindex: int = event["event"]["turnindex"]
        turn_list: list[str] = event["event"]["players"]
        turn_list.append(event["event"]["monster"]["name"])
        print("turnindex", turnindex)
        if turnindex < len(turn_list):
            turn_list[turnindex] = "".join(["-> ", turn_list[turnindex]])
        self.turn_display.set_item_list(turn_list)
