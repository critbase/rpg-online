import pygame
from typing import Callable

import gui.fonts as fonts


class ButtonColors:
    def __init__(self, fg, bg_inactive, bg_active, border, bg_hover):
        self.fg = fg
        self.bg_inactive = bg_inactive
        self.bg_active = bg_active
        self.bg_hover = bg_hover
        self.border = border


COLOR_PRIMARY = ButtonColors("#ffffff", "#0d6efd", "#0a58ca", "#0d6efd", "#0b5ed7")
COLOR_SUCCESS = ButtonColors("#ffffff", "#198754", "#146c43", "#198754", "#157347")
COLOR_LIGHT = ButtonColors("#000000", "#f8f9fa", "#c6c7c8", "#f8f9fa", "#d3d4d5")


class Button:
    HORIZONTAL_PADDING = 5
    VERTICAL_PADDING = 3
    CORNER_RADIUS = 3
    BORDER_WIDTH = 2

    def __init__(
        self,
        surface,
        xy,
        width,
        id,
        label,
        colors: ButtonColors,
        on_click: Callable[["Button"], None] = None,
        font: pygame.font.Font = None,
    ):
        self.surface = surface
        self.xy = xy
        self.width = width
        self.id = id
        self._label = label
        self.colors = colors
        self.on_click = on_click
        self.font = font if font else fonts.normal()
        self._hover = False
        self._active = False
        self._button_surface: pygame.Surface = None
        self._update(label)

    @property
    def label(self) -> str:
        return self._label

    @label.setter
    def label(self, label: str):
        if self._label != label:
            self._update(label)
        self._label = label

    def _update(self, label=None):
        if label is None:
            label = self._label
        if not isinstance(label, pygame.Surface):
            label = self.font.render(label, antialias=True, color=self.colors.fg)
        width = max(
            label.get_width() + 2 * self.HORIZONTAL_PADDING,
            self.width + 2 * self.HORIZONTAL_PADDING,
        )
        button_size = (width, label.get_height() + 2 * self.VERTICAL_PADDING)
        self._button_surface = pygame.surface.Surface(button_size, pygame.SRCALPHA)
        fill_color = (
            self.colors.bg_active
            if self._active
            else self.colors.bg_hover
            if self._hover
            else self.colors.bg_inactive
        )
        pygame.draw.rect(
            surface=self._button_surface,
            color=fill_color,
            rect=((0, 0), button_size),
            border_radius=self.CORNER_RADIUS,
        )
        pygame.draw.rect(
            surface=self._button_surface,
            width=self.BORDER_WIDTH,
            color=self.colors.border,
            rect=((0, 0), button_size),
            border_radius=self.CORNER_RADIUS,
        )
        x_offset = (width - label.get_width()) / 2
        self._button_surface.blit(label, (x_offset, self.VERTICAL_PADDING))

    def consume_ui_event(self, event):
        if pygame.rect.Rect(self.xy, self._button_surface.get_size()).collidepoint(
            event.pos
        ):
            if event.type == pygame.MOUSEBUTTONDOWN:
                self._active = True
                self._update()
            elif event.type == pygame.MOUSEBUTTONUP:
                self._active = False
                self._update()
                if self.on_click:
                    event.target = self
                    self.on_click(self)
            elif event.type == pygame.MOUSEMOTION and not self._hover:
                self._hover = True
                self._update()
            return True
        elif event.type == pygame.MOUSEMOTION and self._hover:
            self._hover = False
            self._active = False
            self._update()
        return False

    def draw(self):
        self.surface.blit(self._button_surface, self.xy)

    def get_size(self):
        return self._button_surface.get_size()

    def get_width(self):
        return self._button_surface.get_width()

    def get_height(self):
        return self._button_surface.get_height()


class ButtonGroup:
    BUTTON_GAP = 5

    def __init__(
        self,
        surface,
        xy,
        on_click: Callable[[Button], None] = None,
        font: pygame.font.Font = None,
    ):
        self.surface = surface
        self.xy = xy
        self.on_click = on_click
        self.font = font if font else fonts.normal()
        self._specs = []
        self._buttons: list[Button] = []
        self._width = 0
        self._height = 0
        self._max_label_width = 0
        self._group: pygame.Surface = None

    def add(
        self, id, label, colors: ButtonColors, on_click: Callable[[Button], None] = None
    ):
        if not isinstance(label, pygame.Surface):
            label = self.font.render(label, True, colors.fg)
        self._specs.append((id, label, colors, on_click if on_click else self.on_click))
        self._max_label_width = max(self._max_label_width, label.get_width())

    def build(self):
        n = len(self._specs)
        self._width = (
            n * (self._max_label_width + 2 * Button.HORIZONTAL_PADDING)
            + (n - 1) * self.BUTTON_GAP
        )
        self._height = self._specs[0][1].get_height() + 2 * Button.VERTICAL_PADDING
        self._group = pygame.Surface((self._width, self._height), pygame.SRCALPHA)
        xy = pygame.math.Vector2(0, 0)
        self._buttons = []
        for spec in self._specs:
            button = Button(self._group, xy.copy(), self._max_label_width, *spec)
            self._buttons.append(button)
            xy += (button.get_width() + self.BUTTON_GAP, 0)

    def consume_ui_event(self, event):
        relative_pos = pygame.math.Vector2(event.pos) - pygame.math.Vector2(self.xy)
        consumed = False
        for button in self._buttons:
            consumed = (
                button.consume_ui_event(
                    pygame.event.Event(event.type, pos=relative_pos)
                )
                or consumed
            )
        return consumed

    def draw(self):
        for button in self._buttons:
            button.draw()
        self.surface.blit(self._group, self.xy)

    def get_width(self):
        return self._width

    def get_height(self):
        return self._height

    def get_size(self):
        return self._width, self._height
