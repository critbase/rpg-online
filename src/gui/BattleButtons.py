import pygame
from typing import Callable

import gui.fonts as fonts
from .client import GameClient
from .button import Button, ButtonColors, COLOR_PRIMARY


class VerticalButtonGroup:
    BUTTON_GAP = 5

    def __init__(
        self,
        surface,
        xy,
        on_click: Callable[[Button], None] = None,
        font: pygame.font.Font = None,
    ):
        self.surface = surface
        self.xy = xy
        self.on_click = on_click
        self.font = font if font else fonts.normal()
        self._specs = []
        self._buttons: list[Button] = []
        self._width = 0
        self._height = 0
        self._max_label_width = 0
        self._group: pygame.Surface = None

    def add(
        self, id, label, colors: ButtonColors, on_click: Callable[[Button], None] = None
    ):
        if not isinstance(label, pygame.Surface):
            label = self.font.render(label, True, colors.fg)
        self._specs.append((id, label, colors, on_click if on_click else self.on_click))
        self._max_label_width = max(self._max_label_width, label.get_width())

    def build(self):
        n = len(self._specs)
        # FIXME: THIS WIDTH IS INCORRECT
        self._width = self._max_label_width
        self._height = (
            n * (self._specs[0][1].get_height() + 2 * Button.VERTICAL_PADDING)
        ) + ((n - 1) * self.BUTTON_GAP)
        self._group = pygame.Surface((self._width, self._height), pygame.SRCALPHA)
        xy = pygame.math.Vector2(0, 0)
        self._buttons = []
        for spec in self._specs:
            button = Button(self._group, xy.copy(), self._max_label_width, *spec)
            self._buttons.append(button)
            xy += (0, button.get_height() + self.BUTTON_GAP)

    def consume_ui_event(self, event):
        relative_pos = pygame.math.Vector2(event.pos) - pygame.math.Vector2(self.xy)
        consumed = False
        for button in self._buttons:
            consumed = (
                button.consume_ui_event(
                    pygame.event.Event(event.type, pos=relative_pos)
                )
                or consumed
            )
        return consumed

    def draw(self):
        for button in self._buttons:
            button.draw()
        self.surface.blit(self._group, self.xy)

    def get_width(self):
        return self._width

    def get_height(self):
        return self._height

    def get_size(self):
        return self._width, self._height


class BattleButtons:
    def __init__(self, surface: pygame.Surface, client: GameClient = None):
        self.client = client

        self._button_group = VerticalButtonGroup(
            surface, (0, 0), on_click=self.handle_click
        )
        self._button_group.add("AAAAAAAA", "AAAAAAAAAA", COLOR_PRIMARY)
        self._button_group.add("BBBBBBB", "BBBBBBBBBBBB", COLOR_PRIMARY)
        self._button_group.add("BBBBBBB", "BBBBBBBBBBBB", COLOR_PRIMARY)
        self._button_group.add("BBBBBBB", "BBBBBBBBBBBB", COLOR_PRIMARY)
        self._button_group.build()

        window_width = surface.get_width()
        window_height = surface.get_height()
        self._button_group.xy = (
            ((window_width - self._button_group.get_width()) / 2) - 50,
            (window_height - self._button_group.get_height()) - 200,
        )
        self._pending_commands: list[str] = []

    def handle_success(self, response: dict):
        command = self._pending_commands.pop(0)
        print(f"ok: command '{command}' completed successfully")

    def handle_error(self, response: dict):
        command = self._pending_commands.pop(0)
        message = response["error"].get("message", "unknown error")
        print(f"error: command '{command}' failed: {message}")

    def handle_click(self, button: Button):
        if self.client:
            self._pending_commands.append(button.id)
            self.client.send(
                {"command": button.id},
                on_success=self.handle_success,
                on_error=self.handle_error,
            )

    def consume_ui_event(self, event):
        if event.type in {
            pygame.MOUSEBUTTONUP,
            pygame.MOUSEBUTTONDOWN,
            pygame.MOUSEMOTION,
        }:
            return self._button_group.consume_ui_event(event)

    def draw(self):
        self._button_group.draw()

    def get_width(self):
        return self._button_group.get_width()

    def get_height(self):
        return self._button_group.get_height()
