import json

import pygame
import gui.fonts as font


class EventView:
    MAX_HISTORY = 16

    def __init__(self, surface, color="black"):
        self.surface = surface
        self.xy = (10, 10)
        self.color = color
        self._font = font.pixel()
        self._rendered_events: list[pygame.Surface] = []

    def append(self, event: dict):
        if len(self._rendered_events) == self.MAX_HISTORY:
            self._rendered_events.pop(0)

        event_text = json.dumps(event)
        rendered_event = self._font.render(event_text, True, self.color)
        self._rendered_events.append(rendered_event)

    def draw(self):
        xy = pygame.math.Vector2(self.xy)
        for rendered_event in self._rendered_events:
            self.surface.blit(rendered_event, xy.copy())
            xy += (0, rendered_event.get_height())
