# This Dockerfile provides an example that you can reuse in your project
# It copies the API source from the API module, and the private key generated
# during the setup (see the top level README for details), placing these
# artifacts into the container's /app directory. It installs the dependencies
# for the API, and configures the container to run the api module by default.

FROM python:3.11-slim
WORKDIR /app
RUN python3 -m pip install flask vtece4564-gamelib==0.1.5
COPY src/api/ /app/api/
COPY private_key.pem /app/
CMD ["/usr/local/bin/python3", "-m", "api"]

# Docker experts will want to optimize the order of the directives in
# this file. Ideally, the RUN directive that installs the dependencies
# should be placed before the COPY directives that install the program
# code. This would make better use of the image cache. However, changing
# the order means that you'll need to pin the version for vtece4564-gamelib
# and you'll need to update the version as needed. In the current ordering
# of the directives, the latest version of the game library will be installed
# each time the image is built.
