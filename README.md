# RPG Online
## Running the game server
Recommended to run using docker-compose. All commands, unless otherwise specified, are run from the root of this repository.


First, generate a key-pair with `python3 -m gameauth`. Set the passphrase to `secret`, or whatever you choose (just change the passphrase in the docker-compose file, under `game-api` in `PRIVATE_KEY_PASSPHRASE`)

Then, you can simply run `docker compose up --build --detach` to start all the requisite components in the background. To bring the server down, run `docker compose down`.

## Running clients
Now, to start some game clients, use the included `launcher` module in this repository.

First, set the `PYTHONPATH` environment variable to the `src` subdirectory:
```bash
# bash
export PYTHONPATH="$PWD/src"
```

Then, run the launcher command, passing it the url of the game server:
```bash
python3 -m launcher --url https://game-api.localhost.devcom.vt.edu alice bob mallory
```

The URL above is specified in the docker compose file (currently unknown how to change the URL).

`alice`, `bob`, and `mallory` are all arguments that specify a player name. Specify as many players as you like, or just one.

TODO: joining an existing game